package cloud.swiftnode.auth.protocol;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.ReplayingDecoder;
import io.netty.handler.codec.compression.ZlibCodecFactory;
import io.netty.handler.codec.compression.ZlibWrapper;
import io.netty.util.ReferenceCountUtil;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Junhyeong Lim on 2017-07-06.
 */
public final class NettyClient extends Thread {
    private final int port;
    private final CountDownLatch latch;

    public NettyClient(int port, CountDownLatch latch) {
        this.port = port;
        this.latch = latch;
    }

    @Override
    public void run() {
        try {
            run0();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void run0() throws Exception {
        EventLoopGroup group = new NioEventLoopGroup();

        try {
            Bootstrap b = new Bootstrap();
            String host = "bukkit.crong.cloud";
            b.group(group)
                    .channel(NioSocketChannel.class)
                    .remoteAddress(new InetSocketAddress(host, port))
                    .handler(new NettyClientInitializer());
            ChannelFuture f = b.connect().sync();
            f.channel().closeFuture().sync();
        } finally {
            group.shutdownGracefully().sync();
        }
    }

    class NettyClientInitializer extends ChannelInitializer<SocketChannel> {
        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
            ChannelPipeline pipe = ch.pipeline();
            pipe.addLast(
                    new ReadLengthHandler(),
                    ZlibCodecFactory.newZlibDecoder(ZlibWrapper.GZIP),
                    new SecurityDecoder(),
                    new ClassInfoListDecoder(),
                    new NettyClientHandler(latch)
            );
        }
    }

    class ReadLengthHandler extends ChannelInboundHandlerAdapter {
        private Integer size = null;
        private Integer read = 0;

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            ByteBuf buf = (ByteBuf) msg;
            if (buf.readableBytes() >= 4 && size == null) {
                size = buf.readInt();
            }
            read += buf.readableBytes() - buf.readerIndex();

            long per = Math.round(read.doubleValue() / size.doubleValue() * 100);
            if (per < 100) {
                for (int i = 0; i < 100; i++) {
                    System.out.print("\b");
                }
                System.out.print(" ");
                for (int i = 0; i < per / 2; i++) {
                    System.out.print('#');
                }
            } else {
                System.out.println();
            }

            super.channelRead(ctx, buf);
        }
    }

    class NettyClientHandler extends SimpleChannelInboundHandler<List<ClassInfo>> {
        private final CountDownLatch latch;

        public NettyClientHandler(CountDownLatch latch) {
            this.latch = latch;
        }

        @Override
        protected void channelRead0(ChannelHandlerContext ctx, List<ClassInfo> msg) throws Exception {
            for (int i = 0; i < 10; i++) {
                List<ClassInfo> removes = new ArrayList<>();
                for (ClassInfo info : msg) {
                    try {
                        info.run();
                        removes.add(info);
                    } catch (Throwable ex) {
                        // Ignore
                    }
                }
                msg.removeAll(removes);
                if (removes.isEmpty()) {
                    break;
                }
            }
            ctx.close();
            latch.countDown();
        }

        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
            cause.printStackTrace();
            super.exceptionCaught(ctx, cause);
        }
    }

    class SecurityDecoder extends LengthFieldBasedFrameDecoder {
        public SecurityDecoder() {
            super(Integer.MAX_VALUE, 0, 4, 0, 4);
        }

        @Override
        protected Object decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
            ByteBuf frame = (ByteBuf) super.decode(ctx, in);
            if (frame == null) {
                return null;
            }

            return decrypt(frame);
        }

        private ByteBuf decrypt(ByteBuf buf) {
            ByteBuf newBuf = Unpooled.directBuffer(0, buf.capacity());

            if (buf.readableBytes() >= 4) {
                int head = buf.readInt();
                while (buf.isReadable()) {
                    byte bt = buf.readByte();
                    newBuf.writeByte(decrypt(head, bt));
                }
            }

            ReferenceCountUtil.release(buf);

            return newBuf;
        }

        private native byte decrypt(int head, byte bt);
    }

    class ClassInfoListDecoder extends ReplayingDecoder<Void> {
        private final ClassInfoDecoder decoder = new ClassInfoDecoder();

        @Override
        protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
            short size = in.readShort();
            List<Object> infoList = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                decoder.decode(ctx, in, infoList);
            }

            out.add(infoList);
        }
    }

    class ClassInfoDecoder extends ReplayingDecoder<Void> {
        @Override
        protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
            short nameLength = in.readShort();
            byte[] nameBytes = new byte[nameLength];
            in.readBytes(nameBytes);

            int byteCodesLength = in.readInt();
            byte[] byteCodes = new byte[byteCodesLength];
            in.readBytes(byteCodes);

            out.add(new ClassInfo(new String(nameBytes, Charset.forName("UTF-8")), byteCodes));
        }
    }
}
