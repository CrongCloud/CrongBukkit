package cloud.swiftnode.auth;

import cloud.swiftnode.auth.protocol.NettyClient;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Junhyeong Lim on 2017-07-09.
 */
public class Main {
    static {
        System.loadLibrary("SwiftBukkitNative");
    }

    public static void main(String[] args) throws Exception {
        CountDownLatch latch = new CountDownLatch(1);
        new NettyClient(2017, latch).start();

        System.out.println(
                "   _                     _ _                   \n" +
                        "  | |                   | (_)                  \n" +
                        "  | |     ___   __ _  __| |_ _ __   __ _       \n" +
                        "  | |    / _ \\ / _` |/ _` | | '_ \\ / _` |      \n" +
                        "  | |___| (_) | (_| | (_| | | | | | (_| |_ _ _ \n" +
                        "  \\_____/\\___/ \\__,_|\\__,_|_|_| |_|\\__, (_|_|_)\n" +
                        "                                    __/ |      \n" +
                        "                                   |___/       \n"
        );
        latch.await();

        org.bukkit.craftbukkit.Main.main(args);
    }
}
