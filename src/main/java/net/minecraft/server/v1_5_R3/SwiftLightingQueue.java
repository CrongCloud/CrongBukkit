package net.minecraft.server.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-30.
 */

import org.bukkit.craftbukkit.v1_5_R3.util.LongObjectHashMap;
import org.spigotmc.SpigotConfig;

import java.util.ArrayDeque;
import java.util.Iterator;

class SwiftLightingQueue {
    private static final long MAX_TIME = (long) (1000000000 / 20 * .95);
    private static int updatesThisTick;

    static void processQueue(long curTime) {
        updatesThisTick = 0;
        final long startTime = System.nanoTime();
        final long maxTickTime = MAX_TIME - (startTime - curTime);
        START:
        for (World world : MinecraftServer.getServer().worlds) {
            if (!SpigotConfig.queueLightUpdates) {
                continue;
            }
            LongObjectHashMap<Chunk> map = ((WorldServer) world).chunkProviderServer.chunks;
            synchronized (map) {
                Iterator<Chunk> iter = map.values().iterator();
                while (iter.hasNext()) {
                    Chunk chunk = iter.next();
                    if (chunk.lightingQueue.processQueue(startTime, maxTickTime)) {
                        break START;
                    }
                }
            }
        }
    }

    static class LightingQueue extends ArrayDeque<Runnable> {
        final private Chunk chunk;

        LightingQueue(Chunk chunk) {
            super();
            this.chunk = chunk;
        }

        @Override
        public boolean add(Runnable runnable) {
            if (SpigotConfig.queueLightUpdates) {
                return super.add(runnable);
            }
            runnable.run();
            return true;
        }

        /**
         * Processes the lighting queue for this chunk
         *
         * @param startTime   If start Time is 0, we will not limit execution time
         * @param maxTickTime Maximum time to spend processing lighting updates
         * @return true to abort processing furthur lighting updates
         */
        private boolean processQueue(long startTime, long maxTickTime) {
            if (this.isEmpty()) {
                return false;
            }
            chunk.world.timings.lightingQueueTimer.startTiming();
            Runnable lightUpdate;
            while ((lightUpdate = this.poll()) != null) {
                lightUpdate.run();
                if (startTime > 0 && ++SwiftLightingQueue.updatesThisTick % 10 == 0 && SwiftLightingQueue.updatesThisTick > 10) {
                    if (System.nanoTime() - startTime > maxTickTime) {
                        return true;
                    }
                }
            }
            chunk.world.timings.lightingQueueTimer.stopTiming();
            return false;
        }

        /**
         * Flushes lighting updates to unload the chunk
         */

        void processUnload() {
            if (!SpigotConfig.queueLightUpdates) {
                return;
            }
            processQueue(0, 0); // No timeout
            final int radius = 1; // TODO: bitflip, why should this ever be 2?
            for (int x = chunk.x - radius; x <= chunk.x + radius; ++x) {
                for (int z = chunk.z - radius; z <= chunk.z + radius; ++z) {
                    if (x == chunk.x && z == chunk.z) {
                        continue;
                    }
                    Chunk neighbor = SwiftUtils.getLoadedChunkWithoutMarkingActive(chunk.world, x, z);
                    if (neighbor != null) {
                        neighbor.lightingQueue.processQueue(0, 0); // No timeout
                    }
                }
            }
        }
    }
}