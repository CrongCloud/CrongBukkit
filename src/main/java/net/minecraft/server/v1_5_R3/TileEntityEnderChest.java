package net.minecraft.server.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-28.
 */

public class TileEntityEnderChest extends TileEntity {
    public float a;
    public float b;
    public int c;
    private int d;

    public TileEntityEnderChest() {
    }

    public void h() {
        super.h();
        if (++this.d % 4 == 0) {
            this.world.playNote(this.x, this.y, this.z, Block.ENDER_CHEST.id, 1, this.c);
        }

        this.b = this.a;
        float var1 = 0.1F;
        double var4;
        if (this.c > 0 && this.a == 0.0F) {
            double var2 = (double) this.x + 0.5D;
            var4 = (double) this.z + 0.5D;
            this.world.makeSound(var2, (double) this.y + 0.5D, var4, "random.chestopen", 0.5F, this.world.random.nextFloat() * 0.1F + 0.9F);
        }

        if (this.c == 0 && this.a > 0.0F || this.c > 0 && this.a < 1.0F) {
            float var6 = this.a;
            if (this.c > 0) {
                this.a += var1;
            } else {
                this.a -= var1;
            }

            if (this.a > 1.0F) {
                this.a = 1.0F;
            }

            float var7 = 0.5F;
            if (this.a < var7 && var6 >= var7) {
                var4 = (double) this.x + 0.5D;
                double var8 = (double) this.z + 0.5D;
                this.world.makeSound(var4, (double) this.y + 0.5D, var8, "random.chestclosed", 0.5F, this.world.random.nextFloat() * 0.1F + 0.9F);
            }

            if (this.a < 0.0F) {
                this.a = 0.0F;
            }
        }

    }

    public boolean b(int var1, int var2) {
        if (var1 == 1) {
            this.c = var2;
            return true;
        } else {
            return super.b(var1, var2);
        }
    }

    public void w_() {
        this.i();
        super.w_();
    }

    public void a() {
        ++this.c;
        this.world.playNote(this.x, this.y, this.z, Block.ENDER_CHEST.id, 1, this.c);
    }

    public void b() {
        --this.c;
        this.world.playNote(this.x, this.y, this.z, Block.ENDER_CHEST.id, 1, this.c);
    }

    public boolean a(EntityHuman var1) {
        return this.world.getTileEntity(this.x, this.y, this.z) == this && var1.e((double) this.x + 0.5D, (double) this.y + 0.5D, (double) this.z + 0.5D) <= 64.0D;
    }
}
