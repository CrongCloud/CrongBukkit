package net.minecraft.server.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-28.
 */

public class TileEntityLightDetector extends TileEntity {
    public TileEntityLightDetector() {
    }

    public void h() {
        if (this.world != null && !this.world.isStatic /*&& this.world.getTime() % 20L == 0L*/) {
            this.q = this.q();
            if (this.q != null && this.q instanceof BlockDaylightDetector) {
                ((BlockDaylightDetector) this.q).i_(this.world, this.x, this.y, this.z);
            }
        }

    }
}
