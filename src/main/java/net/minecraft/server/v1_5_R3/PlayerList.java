package net.minecraft.server.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-28.
 */

import org.bukkit.*;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_5_R3.CraftServer;
import org.bukkit.craftbukkit.v1_5_R3.CraftTravelAgent;
import org.bukkit.craftbukkit.v1_5_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_5_R3.chunkio.ChunkIOExecutor;
import org.bukkit.craftbukkit.v1_5_R3.command.ColouredConsoleSender;
import org.bukkit.craftbukkit.v1_5_R3.command.ConsoleCommandCompleter;
import org.bukkit.craftbukkit.v1_5_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_5_R3.event.CraftEventFactory;
import org.bukkit.entity.Player;
import org.bukkit.event.player.*;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.util.Vector;
import org.spigotmc.SpigotConfig;

import java.io.File;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class PlayerList {
    private static final SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd \'at\' HH:mm:ss z");
    public final List<EntityPlayer> players = new CopyOnWriteArrayList<>();
    public final Map<String, EntityPlayer> playerMap = new java.util.HashMap<String, EntityPlayer>() {
        @Override
        public EntityPlayer put(String key, EntityPlayer value) {
            return super.put(key.toLowerCase(), value);
        }

        @Override
        public EntityPlayer get(Object key) {
            EntityPlayer player = super.get(key instanceof String ? ((String) key).toLowerCase() : key);
            return (player != null && player.playerConnection != null) ? player : null;
        }

        @Override
        public boolean containsKey(Object key) {
            return get(key) != null;
        }

        @Override
        public EntityPlayer remove(Object key) {
            return super.remove(key instanceof String ? ((String) key).toLowerCase() : key);
        }
    };
    private final MinecraftServer server;
    private final BanList banByName = new BanList(new File("banned-players.txt"));
    private final BanList banByIP = new BanList(new File("banned-ips.txt"));
    public IPlayerFileData playerFileData;
    public boolean hasWhitelist;
    protected int maxPlayers;
    protected int c;
    private Set operators = new HashSet();
    private Set whitelist = new LinkedHashSet();
    private EnumGamemode l;
    private boolean m;
    private int n = 0;
    private CraftServer cserver;

    public PlayerList(MinecraftServer minecraftserver) {
        minecraftserver.server = new CraftServer(minecraftserver, this);
        minecraftserver.console = ColouredConsoleSender.getInstance();
        minecraftserver.reader.addCompleter(
                new ConsoleCommandCompleter(minecraftserver.server));
        this.cserver = minecraftserver.server;
        this.server = minecraftserver;
        this.banByName.setEnabled(false);
        this.banByIP.setEnabled(false);
        this.maxPlayers = 8;
    }

    public void a(INetworkManager inetworkmanager, EntityPlayer entityplayer) {
        NBTTagCompound nbttagcompound = this.a(entityplayer);
        entityplayer.spawnIn(this.server.getWorldServer(entityplayer.dimension));
        entityplayer.playerInteractManager.a((WorldServer) entityplayer.world);
        String s = "local";
        if (inetworkmanager.getSocketAddress() != null) {
            s = inetworkmanager.getSocketAddress().toString();
        }

        this.server.getLogger().info(entityplayer.name + "[" + s + "] logged in with entity id " + entityplayer.id + " at ([" + entityplayer.world.worldData.getName() + "] " + entityplayer.locX + ", " + entityplayer.locY + ", " + entityplayer.locZ + ")");
        WorldServer worldserver = this.server.getWorldServer(entityplayer.dimension);
        ChunkCoordinates chunkcoordinates = worldserver.getSpawn();
        this.a(entityplayer, null, worldserver);
        PlayerConnection playerconnection = new PlayerConnection(this.server, inetworkmanager, entityplayer);
        int maxPlayers = this.getMaxPlayers();
        if (maxPlayers > 60) {
            maxPlayers = 60;
        }

        playerconnection.sendPacket(new Packet1Login(entityplayer.id, worldserver.getWorldData().getType(), entityplayer.playerInteractManager.getGameMode(), worldserver.getWorldData().isHardcore(), worldserver.worldProvider.dimension, worldserver.difficulty, worldserver.getHeight(), maxPlayers));
        entityplayer.getBukkitEntity().sendSupportedChannels();
        playerconnection.sendPacket(new Packet6SpawnPosition(chunkcoordinates.x, chunkcoordinates.y, chunkcoordinates.z));
        playerconnection.sendPacket(new Packet202Abilities(entityplayer.abilities));
        playerconnection.sendPacket(new Packet16BlockItemSwitch(entityplayer.inventory.itemInHandIndex));
        this.a((ScoreboardServer) worldserver.getScoreboard(), entityplayer);
        this.b(entityplayer, worldserver);
        this.c(entityplayer);
        playerconnection.a(entityplayer.locX, entityplayer.locY, entityplayer.locZ, entityplayer.yaw, entityplayer.pitch);
        this.server.ae().a(playerconnection);
        playerconnection.sendPacket(new Packet4UpdateTime(worldserver.getTime(), worldserver.getDayTime()));
        if (this.server.getTexturePack().length() > 0) {
            entityplayer.a(this.server.getTexturePack(), this.server.S());
        }

        Iterator iterator = entityplayer.getEffects().iterator();

        while (iterator.hasNext()) {
            MobEffect entity = (MobEffect) iterator.next();
            playerconnection.sendPacket(new Packet41MobEffect(entityplayer.id, entity));
        }

        entityplayer.syncInventory();
        if (nbttagcompound != null && nbttagcompound.hasKey("Riding")) {
            Entity entity1 = EntityTypes.a(nbttagcompound.getCompound("Riding"), worldserver);
            if (entity1 != null) {
                entity1.p = true;
                worldserver.addEntity(entity1);
                entityplayer.mount(entity1);
                entity1.p = false;
            }
        }

    }

    public void a(ScoreboardServer scoreboardserver, EntityPlayer entityplayer) {
        HashSet hashset = new HashSet();
        Iterator iterator = scoreboardserver.getTeams().iterator();

        while (iterator.hasNext()) {
            ScoreboardTeam i = (ScoreboardTeam) iterator.next();
            entityplayer.playerConnection.sendPacket(new Packet209SetScoreboardTeam(i, 0));
        }

        for (int var10 = 0; var10 < 3; ++var10) {
            ScoreboardObjective scoreboardobjective = scoreboardserver.getObjectiveForSlot(var10);
            if (scoreboardobjective != null && !hashset.contains(scoreboardobjective)) {
                List list = scoreboardserver.getScoreboardScorePacketsForObjective(scoreboardobjective);
                Iterator iterator1 = list.iterator();

                while (iterator1.hasNext()) {
                    Packet packet = (Packet) iterator1.next();
                    entityplayer.playerConnection.sendPacket(packet);
                }

                hashset.add(scoreboardobjective);
            }
        }

    }

    public void setPlayerFileData(WorldServer[] aworldserver) {
        if (this.playerFileData == null) {
            this.playerFileData = aworldserver[0].getDataManager().getPlayerFileData();
        }
    }

    public void a(EntityPlayer entityplayer, WorldServer worldserver) {
        WorldServer worldserver1 = entityplayer.o();
        if (worldserver != null) {
            worldserver.getPlayerChunkMap().removePlayer(entityplayer);
        }

        worldserver1.getPlayerChunkMap().addPlayer(entityplayer);
        worldserver1.chunkProviderServer.getChunkAt((int) entityplayer.locX >> 4, (int) entityplayer.locZ >> 4);
    }

    public int a() {
        return PlayerChunkMap.getFurthestViewableBlock(this.o());
    }

    public NBTTagCompound a(EntityPlayer entityplayer) {
        NBTTagCompound nbttagcompound = this.server.worlds.get(0).getWorldData().i();
        NBTTagCompound nbttagcompound1;
        if (entityplayer.getName().equals(this.server.H()) && nbttagcompound != null) {
            entityplayer.f(nbttagcompound);
            nbttagcompound1 = nbttagcompound;
            System.out.println("loading single player");
        } else {
            nbttagcompound1 = this.playerFileData.load(entityplayer);
        }

        return nbttagcompound1;
    }

    protected void b(EntityPlayer entityplayer) {
        entityplayer.lastSave = MinecraftServer.currentTick;
        this.playerFileData.save(entityplayer);
    }

    public void c(EntityPlayer entityplayer) {
        this.cserver.detectListNameConflict(entityplayer);
        this.players.add(entityplayer);
        this.playerMap.put(entityplayer.getName(), entityplayer);
        WorldServer worldserver = this.server.getWorldServer(entityplayer.dimension);
        PlayerJoinEvent playerJoinEvent = new PlayerJoinEvent(this.cserver.getPlayer(entityplayer), "§e" + entityplayer.name + " joined the game.");
        this.cserver.getPluginManager().callEvent(playerJoinEvent);
        String joinMessage = playerJoinEvent.getJoinMessage();
        if (joinMessage != null && joinMessage.length() > 0) {
            this.server.getPlayerList().sendAll(new Packet3Chat(joinMessage));
        }

        this.cserver.onPlayerJoin(playerJoinEvent.getPlayer());
        ChunkIOExecutor.adjustPoolSize(this.getPlayerCount());
        if (entityplayer.world == worldserver && !worldserver.players.contains(entityplayer)) {
            worldserver.addEntity(entityplayer);
            this.a(entityplayer, (WorldServer) null);
        }

        Packet201PlayerInfo packet = new Packet201PlayerInfo(entityplayer.listName, true, 1000);

        int i;
        EntityPlayer entityplayer1;
        for (i = 0; i < this.players.size(); ++i) {
            entityplayer1 = (EntityPlayer) this.players.get(i);
            if (entityplayer1.getBukkitEntity().canSee(entityplayer.getBukkitEntity())) {
                entityplayer1.playerConnection.sendPacket(packet);
            }
        }

        for (i = 0; i < this.players.size(); ++i) {
            entityplayer1 = (EntityPlayer) this.players.get(i);
            if (entityplayer.getBukkitEntity().canSee(entityplayer1.getBukkitEntity())) {
                entityplayer.playerConnection.sendPacket(new Packet201PlayerInfo(entityplayer1.listName, true, entityplayer1.ping));
            }
        }

    }

    public void d(EntityPlayer entityplayer) {
        entityplayer.o().getPlayerChunkMap().movePlayer(entityplayer);
    }

    public String disconnect(EntityPlayer entityplayer) {
        if (entityplayer.playerConnection.disconnected) {
            return null;
        } else {
            CraftEventFactory.handleInventoryCloseEvent(entityplayer);
            PlayerQuitEvent playerQuitEvent = new PlayerQuitEvent(this.cserver.getPlayer(entityplayer), "§e" + entityplayer.name + " left the game.");
            this.cserver.getPluginManager().callEvent(playerQuitEvent);
            entityplayer.getBukkitEntity().disconnect(playerQuitEvent.getQuitMessage());
            this.b(entityplayer);
            WorldServer worldserver = entityplayer.o();
            if (entityplayer.vehicle != null) {
                worldserver.kill(entityplayer.vehicle);
                System.out.println("removing player mount");
            }

            worldserver.kill(entityplayer);
            worldserver.getPlayerChunkMap().removePlayer(entityplayer);
            this.players.remove(entityplayer);
            this.playerMap.remove(entityplayer.getName());
            ChunkIOExecutor.adjustPoolSize(this.getPlayerCount());
            Packet201PlayerInfo packet = new Packet201PlayerInfo(entityplayer.listName, false, 9999);

            for (int i = 0; i < this.players.size(); ++i) {
                EntityPlayer entityplayer1 = (EntityPlayer) this.players.get(i);
                if (entityplayer1.getBukkitEntity().canSee(entityplayer.getBukkitEntity())) {
                    entityplayer1.playerConnection.sendPacket(packet);
                }
            }

            this.cserver.getScoreboardManager().removePlayer(entityplayer.getBukkitEntity());
            return playerQuitEvent.getQuitMessage();
        }
    }

    public EntityPlayer attemptLogin(PendingConnection pendingconnection, String s, String hostname) {
        EntityPlayer entity = new EntityPlayer(this.server, this.server.getWorldServer(0), s, this.server.M() ? new DemoPlayerInteractManager(this.server.getWorldServer(0)) : new PlayerInteractManager(this.server.getWorldServer(0)));
        CraftPlayer player = entity.getBukkitEntity();
        PlayerLoginEvent event = new PlayerLoginEvent(player, hostname, ((InetSocketAddress) pendingconnection.networkManager.getSocketAddress()).getAddress(), pendingconnection.getSocket().getInetAddress());
        SocketAddress socketaddress = pendingconnection.networkManager.getSocketAddress();
        if (this.banByName.isBanned(s)) {
            BanEntry s2 = (BanEntry) this.banByName.getEntries().get(s);
            String banentry1 = "You are banned from this server!\nReason: " + s2.getReason();
            if (s2.getExpires() != null) {
                banentry1 = banentry1 + "\nYour ban will be removed on " + d.format(s2.getExpires());
            }

            event.disallow(Result.KICK_BANNED, banentry1);
        } else if (!this.isWhitelisted(s)) {
            event.disallow(Result.KICK_WHITELIST, SpigotConfig.whitelistMessage);
        } else {
            String s21 = socketaddress.toString();
            s21 = s21.substring(s21.indexOf("/") + 1);
            s21 = s21.substring(0, s21.indexOf(":"));
            if (this.banByIP.isBanned(s21)) {
                BanEntry banentry11 = (BanEntry) this.banByIP.getEntries().get(s21);
                String s3 = "Your IP address is banned from this server!\nReason: " + banentry11.getReason();
                if (banentry11.getExpires() != null) {
                    s3 = s3 + "\nYour ban will be removed on " + d.format(banentry11.getExpires());
                }

                event.disallow(Result.KICK_BANNED, s3);
            } else if (this.players.size() >= this.maxPlayers) {
                event.disallow(Result.KICK_FULL, SpigotConfig.serverFullMessage);
            } else {
                event.disallow(Result.ALLOWED, s21);
            }
        }

        this.cserver.getPluginManager().callEvent(event);
        if (event.getResult() != Result.ALLOWED) {
            pendingconnection.disconnect(event.getKickMessage());
            return null;
        } else {
            return entity;
        }
    }

    public EntityPlayer processLogin(EntityPlayer player) {
        String s = player.name;
        ArrayList arraylist = new ArrayList();

        EntityPlayer entityplayer;
        /*
        for (int iterator = 0; iterator < this.players.size(); ++iterator) {
            entityplayer = (EntityPlayer) this.players.get(iterator);
            if (entityplayer.name.equalsIgnoreCase(s)) {
                arraylist.add(entityplayer);
            }
        }


        Iterator var6 = arraylist.iterator();

        while (var6.hasNext()) {
        */
        if ((entityplayer = playerMap.get(s)) != null) {
            entityplayer.playerConnection.disconnect("You logged in from another location");
        }

        return player;
    }

    public EntityPlayer moveToWorld(EntityPlayer entityplayer, int i, boolean flag) {
        return this.moveToWorld(entityplayer, i, flag, null, true);
    }

    public EntityPlayer moveToWorld(EntityPlayer entityplayer, int i, boolean flag, Location location, boolean avoidSuffocation) {
        entityplayer.o().getTracker().untrackPlayer(entityplayer);
        entityplayer.o().getPlayerChunkMap().removePlayer(entityplayer);
        this.players.remove(entityplayer);
        this.server.getWorldServer(entityplayer.dimension).removeEntity(entityplayer);
        ChunkCoordinates chunkcoordinates = entityplayer.getBed();
        boolean flag1 = entityplayer.isRespawnForced();
        EntityPlayer entityplayer1 = entityplayer;
        World fromWorld = entityplayer.getBukkitEntity().getWorld();
        entityplayer.viewingCredits = false;
        entityplayer.copyTo(entityplayer, flag);
        ChunkCoordinates chunkcoordinates1;
        if (location == null) {
            boolean worldserver = false;
            CraftWorld actualDimension = (CraftWorld) this.server.server.getWorld(entityplayer.spawnWorld);
            if (actualDimension != null && chunkcoordinates != null) {
                chunkcoordinates1 = EntityHuman.getBed(actualDimension.getHandle(), chunkcoordinates, flag1);
                if (chunkcoordinates1 != null) {
                    worldserver = true;
                    location = new Location(actualDimension, (double) chunkcoordinates1.x + 0.5D, (double) chunkcoordinates1.y, (double) chunkcoordinates1.z + 0.5D);
                } else {
                    entityplayer.setRespawnPosition(null, true);
                    entityplayer.playerConnection.sendPacket(new Packet70Bed(0, 0));
                }
            }

            if (location == null) {
                actualDimension = (CraftWorld) this.server.server.getWorlds().get(0);
                chunkcoordinates = actualDimension.getHandle().getSpawn();
                location = new Location(actualDimension, (double) chunkcoordinates.x + 0.5D, (double) chunkcoordinates.y, (double) chunkcoordinates.z + 0.5D);
            }

            Player iterator = this.cserver.getPlayer(entityplayer);
            PlayerRespawnEvent event = new PlayerRespawnEvent(iterator, location, worldserver);
            this.cserver.getPluginManager().callEvent(event);
            location = event.getRespawnLocation();
            entityplayer.reset();
        } else {
            location.setWorld(this.server.getWorldServer(i).getWorld());
        }

        WorldServer worldserver1 = ((CraftWorld) location.getWorld()).getHandle();
        entityplayer.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
        worldserver1.chunkProviderServer.getChunkAt((int) entityplayer.locX >> 4, (int) entityplayer.locZ >> 4);

        while (avoidSuffocation && !worldserver1.getCubes(entityplayer1, entityplayer1.boundingBox).isEmpty()) {
            entityplayer1.setPosition(entityplayer1.locX, entityplayer1.locY + 1.0D, entityplayer1.locZ);
        }

        byte actualDimension1 = (byte) worldserver1.getWorld().getEnvironment().getId();
        entityplayer1.playerConnection.sendPacket(new Packet9Respawn((byte) (actualDimension1 >= 0 ? -1 : 0), (byte) worldserver1.difficulty, worldserver1.getWorldData().getType(), worldserver1.getHeight(), entityplayer.playerInteractManager.getGameMode()));
        entityplayer1.playerConnection.sendPacket(new Packet9Respawn(actualDimension1, (byte) worldserver1.difficulty, worldserver1.getWorldData().getType(), worldserver1.getHeight(), entityplayer.playerInteractManager.getGameMode()));
        entityplayer1.spawnIn(worldserver1);
        entityplayer1.dead = false;
        entityplayer1.playerConnection.teleport(new Location(worldserver1.getWorld(), entityplayer1.locX, entityplayer1.locY, entityplayer1.locZ, entityplayer1.yaw, entityplayer1.pitch));
        entityplayer1.setSneaking(false);
        chunkcoordinates1 = worldserver1.getSpawn();
        entityplayer1.playerConnection.sendPacket(new Packet6SpawnPosition(chunkcoordinates1.x, chunkcoordinates1.y, chunkcoordinates1.z));
        entityplayer1.playerConnection.sendPacket(new Packet43SetExperience(entityplayer1.exp, entityplayer1.expTotal, entityplayer1.expLevel));
        this.b(entityplayer1, worldserver1);
        worldserver1.getPlayerChunkMap().addPlayer(entityplayer1);
        worldserver1.addEntity(entityplayer1);
        this.players.add(entityplayer1);
        this.updateClient(entityplayer1);
        entityplayer1.updateAbilities();
        Iterator iterator1 = entityplayer1.getEffects().iterator();

        while (iterator1.hasNext()) {
            MobEffect event1 = (MobEffect) iterator1.next();
            entityplayer1.playerConnection.sendPacket(new Packet41MobEffect(entityplayer1.id, event1));
        }

        entityplayer1.setHealth(entityplayer1.getHealth());
        if (fromWorld != location.getWorld()) {
            PlayerChangedWorldEvent event2 = new PlayerChangedWorldEvent(entityplayer1.getBukkitEntity(), fromWorld);
            Bukkit.getServer().getPluginManager().callEvent(event2);
            // Swiftnode
            new Thread(() -> {
                MinecraftServer server = MinecraftServer.getServer();
                Integer savePeriod = SpigotConfig.worldSavePeriod;
                server.serverAutoSave = (savePeriod > 0 && server.ticks % savePeriod == 0);
                int playerSaveInterval = SpigotConfig.playerAutoSaveRate;
                if (playerSaveInterval < 0) {
                    playerSaveInterval = SpigotConfig.worldSavePeriod;
                }
                if (playerSaveInterval > 0) {
                    server.getPlayerList().savePlayers(playerSaveInterval);
                }
            }).start();
            //
        }

        return entityplayer1;
    }

    public void changeDimension(EntityPlayer entityplayer, int i, TeleportCause cause) {
        WorldServer exitWorld = null;
        if (entityplayer.dimension < 10) {
            Iterator enter = this.server.worlds.iterator();

            while (enter.hasNext()) {
                WorldServer exit = (WorldServer) enter.next();
                if (exit.dimension == i) {
                    exitWorld = exit;
                }
            }
        }

        Location enter1 = entityplayer.getBukkitEntity().getLocation();
        Location exit1 = null;
        boolean useTravelAgent = false;
        if (exitWorld != null) {
            if (cause == TeleportCause.END_PORTAL && i == 0) {
                exit1 = entityplayer.getBukkitEntity().getBedSpawnLocation();
                if (exit1 == null || ((CraftWorld) exit1.getWorld()).getHandle().dimension != 0) {
                    exit1 = exitWorld.getWorld().getSpawnLocation();
                }
            } else {
                exit1 = this.calculateTarget(enter1, exitWorld);
                useTravelAgent = true;
            }
        }

        TravelAgent agent = exit1 != null ? (TravelAgent) ((CraftWorld) exit1.getWorld()).getHandle().t() : CraftTravelAgent.DEFAULT;
        PlayerPortalEvent event = new PlayerPortalEvent(entityplayer.getBukkitEntity(), enter1, exit1, agent, cause);
        event.useTravelAgent(useTravelAgent);
        Bukkit.getServer().getPluginManager().callEvent(event);
        if (!event.isCancelled() && event.getTo() != null) {
            exit1 = event.useTravelAgent() ? event.getPortalTravelAgent().findOrCreate(event.getTo()) : event.getTo();
            if (exit1 != null) {
                exitWorld = ((CraftWorld) exit1.getWorld()).getHandle();
                Vector velocity = entityplayer.getBukkitEntity().getVelocity();
                boolean before = exitWorld.chunkProviderServer.forceChunkLoad;
                exitWorld.chunkProviderServer.forceChunkLoad = true;
                exitWorld.t().adjustExit(entityplayer, exit1, velocity);
                exitWorld.chunkProviderServer.forceChunkLoad = before;
                this.moveToWorld(entityplayer, exitWorld.dimension, true, exit1, false);
                if (entityplayer.motX != velocity.getX() || entityplayer.motY != velocity.getY() || entityplayer.motZ != velocity.getZ()) {
                    entityplayer.getBukkitEntity().setVelocity(velocity);
                }

            }
        }
    }

    public void a(Entity entity, int i, WorldServer worldserver, WorldServer worldserver1) {
        Location exit = this.calculateTarget(entity.getBukkitEntity().getLocation(), worldserver1);
        this.repositionEntity(entity, exit, true);
    }

    public Location calculateTarget(Location enter, net.minecraft.server.v1_5_R3.World target) {
        WorldServer worldserver = ((CraftWorld) enter.getWorld()).getHandle();
        WorldServer worldserver1 = target.getWorld().getHandle();
        int i = worldserver.dimension;
        double y = enter.getY();
        float yaw = enter.getYaw();
        float pitch = enter.getPitch();
        double d0 = enter.getX();
        double d1 = enter.getZ();
        double d2 = 8.0D;
        if (worldserver1.dimension == -1) {
            d0 /= d2;
            d1 /= d2;
        } else if (worldserver1.dimension == 0) {
            d0 *= d2;
            d1 *= d2;
        } else {
            ChunkCoordinates chunkcoordinates;
            if (i == 1) {
                worldserver1 = this.server.worlds.get(0);
                chunkcoordinates = worldserver1.getSpawn();
            } else {
                chunkcoordinates = worldserver1.getDimensionSpawn();
            }

            d0 = (double) chunkcoordinates.x;
            y = (double) chunkcoordinates.y;
            d1 = (double) chunkcoordinates.z;
            yaw = 90.0F;
            pitch = 0.0F;
        }

        if (i != 1) {
            d0 = (double) MathHelper.a((int) d0, -29999872, 29999872);
            d1 = (double) MathHelper.a((int) d1, -29999872, 29999872);
        }

        return new Location(worldserver1.getWorld(), d0, y, d1, yaw, pitch);
    }

    public void repositionEntity(Entity entity, Location exit, boolean portal) {
        int i = entity.dimension;
        WorldServer worldserver = (WorldServer) entity.world;
        WorldServer worldserver1 = ((CraftWorld) exit.getWorld()).getHandle();
        worldserver.methodProfiler.a("moving");
        entity.setPositionRotation(exit.getX(), exit.getY(), exit.getZ(), exit.getYaw(), exit.getPitch());
        if (entity.isAlive()) {
            worldserver.entityJoinedWorld(entity, false);
        }

        worldserver.methodProfiler.b();
        if (i != 1) {
            worldserver.methodProfiler.a("placing");
            if (entity.isAlive()) {
                worldserver1.addEntity(entity);
                worldserver1.entityJoinedWorld(entity, false);
                if (portal) {
                    Vector velocity = entity.getBukkitEntity().getVelocity();
                    worldserver1.t().adjustExit(entity, exit, velocity);
                    entity.setPositionRotation(exit.getX(), exit.getY(), exit.getZ(), exit.getYaw(), exit.getPitch());
                    if (entity.motX != velocity.getX() || entity.motY != velocity.getY() || entity.motZ != velocity.getZ()) {
                        entity.getBukkitEntity().setVelocity(velocity);
                    }
                }
            }

            worldserver.methodProfiler.b();
        }

        entity.spawnIn(worldserver1);
    }

    public void tick() {
        if (++this.n > 600) {
            this.n = 0;
        }

    }

    public void sendAll(Packet packet) {
        for (int i = 0; i < this.players.size(); ++i) {
            ((EntityPlayer) this.players.get(i)).playerConnection.sendPacket(packet);
        }

    }

    public void a(Packet packet, int i) {
        for (int j = 0; j < this.players.size(); ++j) {
            EntityPlayer entityplayer = (EntityPlayer) this.players.get(j);
            if (entityplayer.dimension == i) {
                entityplayer.playerConnection.sendPacket(packet);
            }
        }

    }

    public String c() {
        String s = "";

        for (int i = 0; i < this.players.size(); ++i) {
            if (i > 0) {
                s = s + ", ";
            }

            s = s + ((EntityPlayer) this.players.get(i)).name;
        }

        return s;
    }

    public String[] d() {
        String[] astring = new String[this.players.size()];

        for (int i = 0; i < this.players.size(); ++i) {
            astring[i] = ((EntityPlayer) this.players.get(i)).name;
        }

        return astring;
    }

    public BanList getNameBans() {
        return this.banByName;
    }

    public BanList getIPBans() {
        return this.banByIP;
    }

    public void addOp(String s) {
        this.operators.add(s.toLowerCase());
        Player player = this.server.server.getPlayer(s);
        if (player != null) {
            player.recalculatePermissions();
        }

    }

    public void removeOp(String s) {
        this.operators.remove(s.toLowerCase());
        Player player = this.server.server.getPlayer(s);
        if (player != null) {
            player.recalculatePermissions();
        }

    }

    public boolean isWhitelisted(String s) {
        s = s.trim().toLowerCase();
        return !this.hasWhitelist || this.operators.contains(s) || this.whitelist.contains(s);
    }

    public boolean isOp(String s) {
        return this.operators.contains(s.trim().toLowerCase()) || this.server.I() && this.server.worlds.get(0).getWorldData().allowCommands() && this.server.H().equalsIgnoreCase(s) || this.m;
    }

    public EntityPlayer getPlayer(String s) {
        if (true) {
            return playerMap.get(s);
        }
        Iterator iterator = this.players.iterator();

        while (iterator.hasNext()) {
            EntityPlayer entityplayer = (EntityPlayer) iterator.next();
            if (entityplayer.name.equalsIgnoreCase(s)) {
                return entityplayer;
            }
        }

        return null;
    }

    public List a(ChunkCoordinates chunkcoordinates, int i, int j, int k, int l, int i1, int j1, Map map, String s, String s1) {
        if (this.players.isEmpty()) {
            return null;
        } else {
            Object object = new ArrayList();
            boolean flag = k < 0;
            int k1 = i * i;
            int l1 = j * j;
            k = MathHelper.a(k);

            for (int i2 = 0; i2 < this.players.size(); ++i2) {
                EntityPlayer entityplayer = (EntityPlayer) this.players.get(i2);
                boolean flag1;
                if (s != null) {
                    flag1 = s.startsWith("!");
                    if (flag1) {
                        s = s.substring(1);
                    }

                    if (flag1 == s.equalsIgnoreCase(entityplayer.getLocalizedName())) {
                        continue;
                    }
                }

                if (s1 != null) {
                    flag1 = s1.startsWith("!");
                    if (flag1) {
                        s1 = s1.substring(1);
                    }

                    ScoreboardTeam f = entityplayer.getScoreboardTeam();
                    String s2 = f == null ? "" : f.getName();
                    if (flag1 == s1.equalsIgnoreCase(s2)) {
                        continue;
                    }
                }

                if (chunkcoordinates != null && (i > 0 || j > 0)) {
                    float var20 = chunkcoordinates.e(entityplayer.b());
                    if (i > 0 && var20 < (float) k1 || j > 0 && var20 > (float) l1) {
                        continue;
                    }
                }

                if (this.a(entityplayer, map) && (l == EnumGamemode.NONE.a() || l == entityplayer.playerInteractManager.getGameMode().a()) && (i1 <= 0 || entityplayer.expLevel >= i1) && entityplayer.expLevel <= j1) {
                    ((List) object).add(entityplayer);
                }
            }

            if (chunkcoordinates != null) {
                Collections.sort((List) object, new PlayerDistanceComparator(chunkcoordinates));
            }

            if (flag) {
                Collections.reverse((List) object);
            }

            if (k > 0) {
                object = ((List) object).subList(0, Math.min(k, ((List) object).size()));
            }

            return (List) object;
        }
    }

    private boolean a(EntityHuman entityhuman, Map map) {
        if (map != null && map.size() != 0) {
            Iterator iterator = map.entrySet().iterator();

            Entry entry;
            boolean flag;
            int i;
            do {
                if (!iterator.hasNext()) {
                    return true;
                }

                entry = (Entry) iterator.next();
                String s = (String) entry.getKey();
                flag = false;
                if (s.endsWith("_min") && s.length() > 4) {
                    flag = true;
                    s = s.substring(0, s.length() - 4);
                }

                Scoreboard scoreboard = entityhuman.getScoreboard();
                ScoreboardObjective scoreboardobjective = scoreboard.getObjective(s);
                if (scoreboardobjective == null) {
                    return false;
                }

                ScoreboardScore scoreboardscore = entityhuman.getScoreboard().getPlayerScoreForObjective(entityhuman.getLocalizedName(), scoreboardobjective);
                i = scoreboardscore.getScore();
                if (i < ((Integer) entry.getValue()).intValue() && flag) {
                    return false;
                }
            } while (i <= ((Integer) entry.getValue()).intValue() || flag);

            return false;
        } else {
            return true;
        }
    }

    public void sendPacketNearby(double d0, double d1, double d2, double d3, int i, Packet packet) {
        this.sendPacketNearby(null, d0, d1, d2, d3, i, packet);
    }

    public void sendPacketNearby(EntityHuman entityhuman, double d0, double d1, double d2, double d3, int i, Packet packet) {
        for (int j = 0; j < this.players.size(); ++j) {
            EntityPlayer entityplayer = (EntityPlayer) this.players.get(j);
            if ((entityhuman == null || !(entityhuman instanceof EntityPlayer) || entityplayer.getBukkitEntity().canSee(((EntityPlayer) entityhuman).getBukkitEntity())) && entityplayer != entityhuman && entityplayer.dimension == i) {
                double d4 = d0 - entityplayer.locX;
                double d5 = d1 - entityplayer.locY;
                double d6 = d2 - entityplayer.locZ;
                if (d4 * d4 + d5 * d5 + d6 * d6 < d3 * d3) {
                    entityplayer.playerConnection.sendPacket(packet);
                }
            }
        }

    }

    public void savePlayers() {
        savePlayers(null);
    }

    public void savePlayers(Integer interval) {
        long now = MinecraftServer.currentTick;
        for (int i = 0; i < this.players.size(); ++i) {
            EntityPlayer entityplayer = this.players.get(i);
            if (interval == null || now - entityplayer.lastSave >= interval) {
                this.b(entityplayer);
            }
        }
    }

    public void addWhitelist(String s) {
        this.whitelist.add(s);
    }

    public void removeWhitelist(String s) {
        this.whitelist.remove(s);
    }

    public Set getWhitelisted() {
        return this.whitelist;
    }

    public Set getOPs() {
        return this.operators;
    }

    public void reloadWhitelist() {
    }

    public void b(EntityPlayer entityplayer, WorldServer worldserver) {
        entityplayer.playerConnection.sendPacket(new Packet4UpdateTime(worldserver.getTime(), worldserver.getDayTime()));
        if (worldserver.P()) {
            entityplayer.setPlayerWeather(WeatherType.DOWNFALL, false);
        }

    }

    public void updateClient(EntityPlayer entityplayer) {
        entityplayer.updateInventory(entityplayer.defaultContainer);
        entityplayer.triggerHealthUpdate();
        entityplayer.playerConnection.sendPacket(new Packet16BlockItemSwitch(entityplayer.inventory.itemInHandIndex));
    }

    public int getPlayerCount() {
        return this.players.size();
    }

    public int getMaxPlayers() {
        return this.maxPlayers;
    }

    public String[] getSeenPlayers() {
        return this.server.worlds.get(0).getDataManager().getPlayerFileData().getSeenPlayers();
    }

    public boolean getHasWhitelist() {
        return this.hasWhitelist;
    }

    public void setHasWhitelist(boolean flag) {
        this.hasWhitelist = flag;
    }

    public List j(String s) {
        ArrayList arraylist = new ArrayList();
        Iterator iterator = this.players.iterator();

        while (iterator.hasNext()) {
            EntityPlayer entityplayer = (EntityPlayer) iterator.next();
            if (entityplayer.p().equals(s)) {
                arraylist.add(entityplayer);
            }
        }

        return arraylist;
    }

    public int o() {
        return this.c;
    }

    public MinecraftServer getServer() {
        return this.server;
    }

    public NBTTagCompound q() {
        return null;
    }

    private void a(EntityPlayer entityplayer, EntityPlayer entityplayer1, net.minecraft.server.v1_5_R3.World world) {
        if (entityplayer1 != null) {
            entityplayer.playerInteractManager.setGameMode(entityplayer1.playerInteractManager.getGameMode());
        } else if (this.l != null) {
            entityplayer.playerInteractManager.setGameMode(this.l);
        }

        entityplayer.playerInteractManager.b(world.getWorldData().getGameType());
    }

    public void r() {
        while (!this.players.isEmpty()) {
            EntityPlayer p = (EntityPlayer) this.players.get(0);
            p.playerConnection.disconnect(this.server.server.getShutdownMessage());
            if (!this.players.isEmpty() && this.players.get(0) == p) {
                this.players.remove(0);
            }
        }

    }

    public void k(String s) {
        this.server.info(s);
        this.sendAll(new Packet3Chat(s));
    }
}
