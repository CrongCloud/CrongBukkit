package net.minecraft.server.v1_5_R3;

import org.bukkit.craftbukkit.Main;
import org.bukkit.craftbukkit.libs.jline.console.ConsoleReader;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Junhyeong Lim on 2017-06-23.
 */
class ThreadCommandReader extends Thread {
    final DedicatedServer server;

    ThreadCommandReader(DedicatedServer dedicatedserver) {
        super("Command Reader");
        this.server = dedicatedserver;
    }

    public void run() {
        if (Main.useConsole) {
            ConsoleReader reader = this.server.reader;

            try {
                while (!this.server.isStopped() && this.server.isRunning()) {
                    String msg;
                    if (Main.useJline) {
                        msg = reader.readLine("> ", null);
                    } else {
                        msg = reader.readLine();
                    }

                    if (msg != null) {
                        this.server.issueCommand(msg, this.server);
                    }
                }
            } catch (IOException var4) {
                Logger.getLogger("").log(Level.SEVERE, null, var4);
            }

        }
    }
}

