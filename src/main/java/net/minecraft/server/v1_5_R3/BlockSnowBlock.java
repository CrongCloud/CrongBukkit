package net.minecraft.server.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-31.
 */

import java.util.Random;

public class BlockSnowBlock extends Block {
    protected BlockSnowBlock(int var1) {
        super(var1, Material.SNOW_BLOCK);
//        this.b(true);
        this.a(CreativeModeTab.b);
    }

    public int getDropType(int var1, Random var2, int var3) {
        return Item.SNOW_BALL.id;
    }

    public int a(Random var1) {
        return 4;
    }
    /*
    public void a(World var1, int var2, int var3, int var4, Random var5) {
        if(var1.b(EnumSkyBlock.BLOCK, var2, var3, var4) > 11) {
            this.c(var1, var2, var3, var4, var1.getData(var2, var3, var4), 0);
            var1.setAir(var2, var3, var4);
        }
    }
    */
}
