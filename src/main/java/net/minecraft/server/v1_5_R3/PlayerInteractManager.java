package net.minecraft.server.v1_5_R3;

import org.bukkit.craftbukkit.v1_5_R3.event.CraftEventFactory;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.Event.Result;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.InventoryHolder;

import java.util.List;

public class PlayerInteractManager {
    public World world;
    public EntityPlayer player;
    private EnumGamemode gamemode;
    private boolean d;
    private int lastDigTick;
    private int f;
    private int g;
    private int h;
    private int currentTick;
    private boolean j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;

    public PlayerInteractManager(World world) {
        this.gamemode = EnumGamemode.NONE;
        this.o = -1;
        this.world = world;
    }

    public EnumGamemode getGameMode() {
        return this.gamemode;
    }

    public void setGameMode(EnumGamemode enumgamemode) {
        this.gamemode = enumgamemode;
        enumgamemode.a(this.player.abilities);
        this.player.updateAbilities();
    }

    public boolean isCreative() {
        return this.gamemode.d();
    }

    public void b(EnumGamemode enumgamemode) {
        if (this.gamemode == EnumGamemode.NONE) {
            this.gamemode = enumgamemode;
        }

        this.setGameMode(this.gamemode);
    }

    public void a() {
        this.currentTick = MinecraftServer.currentTick;
        int i;
        float f;
        int j;
        if (this.j) {
            i = this.currentTick - this.n;
            int block1 = this.world.getTypeId(this.k, this.l, this.m);
            if (block1 == 0) {
                this.j = false;
            } else {
                Block l = Block.byId[block1];
                f = l.getDamage(this.player, this.player.world, this.k, this.l, this.m) * (float) (i + 1);
                j = (int) (f * 10.0F);
                if (j != this.o) {
                    this.world.f(this.player.id, this.k, this.l, this.m, j);
                    this.o = j;
                }

                if (f >= 1.0F) {
                    this.j = false;
                    this.breakBlock(this.k, this.l, this.m);
                }
            }
        } else if (this.d) {
            i = this.world.getTypeId(this.f, this.g, this.h);
            Block block11 = Block.byId[i];
            if (block11 == null) {
                this.world.f(this.player.id, this.f, this.g, this.h, -1);
                this.o = -1;
                this.d = false;
            } else {
                int l1 = this.currentTick - this.lastDigTick;
                f = block11.getDamage(this.player, this.player.world, this.f, this.g, this.h) * (float) (l1 + 1);
                j = (int) (f * 10.0F);
                if (j != this.o) {
                    this.world.f(this.player.id, this.f, this.g, this.h, j);
                    this.o = j;
                }
            }
        }

    }

    public void dig(int i, int j, int k, int l) {
        PlayerInteractEvent event = CraftEventFactory.callPlayerInteractEvent(this.player, Action.LEFT_CLICK_BLOCK, i, j, k, l, this.player.inventory.getItemInHand());
        if (!this.gamemode.isAdventure() || this.player.e(i, j, k)) {
            if (event.isCancelled()) {
                this.player.playerConnection.sendPacket(new Packet53BlockChange(i, j, k, this.world));
                TileEntity f1 = this.world.getTileEntity(i, j, k);
                if (f1 != null) {
                    this.player.playerConnection.sendPacket(f1.getUpdatePacket());
                }

                return;
            }

            if (this.isCreative()) {
                if (!this.world.douseFire(null, i, j, k, l)) {
                    this.breakBlock(i, j, k);
                }
            } else {
                this.world.douseFire(null, i, j, k, l);
                this.lastDigTick = this.currentTick;
                float f = 1.0F;
                int i1 = this.world.getTypeId(i, j, k);
                if (event.useInteractedBlock() == Result.DENY) {
                    if (i1 == Block.WOODEN_DOOR.id) {
                        boolean blockEvent = (this.world.getData(i, j, k) & 8) == 0;
                        this.player.playerConnection.sendPacket(new Packet53BlockChange(i, j, k, this.world));
                        this.player.playerConnection.sendPacket(new Packet53BlockChange(i, j + (blockEvent ? 1 : -1), k, this.world));
                    } else if (i1 == Block.TRAP_DOOR.id) {
                        this.player.playerConnection.sendPacket(new Packet53BlockChange(i, j, k, this.world));
                    }
                } else if (i1 > 0) {
                    Block.byId[i1].attack(this.world, i, j, k, this.player);
                    this.world.douseFire(null, i, j, k, l);
                }

                if (i1 > 0) {
                    f = Block.byId[i1].getDamage(this.player, this.world, i, j, k);
                }

                if (event.useItemInHand() == Result.DENY) {
                    if (f > 1.0F) {
                        this.player.playerConnection.sendPacket(new Packet53BlockChange(i, j, k, this.world));
                    }

                    return;
                }

                BlockDamageEvent blockEvent1 = CraftEventFactory.callBlockDamageEvent(this.player, i, j, k, this.player.inventory.getItemInHand(), f >= 1.0F);
                if (blockEvent1.isCancelled()) {
                    this.player.playerConnection.sendPacket(new Packet53BlockChange(i, j, k, this.world));
                    return;
                }

                if (blockEvent1.getInstaBreak()) {
                    f = 2.0F;
                }

                if (i1 > 0 && f >= 1.0F) {
                    this.breakBlock(i, j, k);
                } else {
                    this.d = true;
                    this.f = i;
                    this.g = j;
                    this.h = k;
                    int j1 = (int) (f * 10.0F);
                    this.world.f(this.player.id, i, j, k, j1);
                    this.o = j1;
                }
            }

            this.world.spigotConfig.antiXrayInstance.updateNearbyBlocks(this.world, i, j, k);
        }

    }

    public void a(int i, int j, int k) {
        if (i == this.f && j == this.g && k == this.h) {
            this.currentTick = MinecraftServer.currentTick;
            int l = this.currentTick - this.lastDigTick;
            int i1 = this.world.getTypeId(i, j, k);
            if (i1 != 0) {
                Block block = Block.byId[i1];
                float f = block.getDamage(this.player, this.player.world, i, j, k) * (float) (l + 1);
                if (f >= 0.7F) {
                    this.d = false;
                    this.world.f(this.player.id, i, j, k, -1);
                    this.breakBlock(i, j, k);
                } else if (!this.j) {
                    this.d = false;
                    this.j = true;
                    this.k = i;
                    this.l = j;
                    this.m = k;
                    this.n = this.lastDigTick;
                }
            }
        } else {
            this.player.playerConnection.sendPacket(new Packet53BlockChange(i, j, k, this.world));
        }

    }

    public void c(int i, int j, int k) {
        this.d = false;
        this.world.f(this.player.id, this.f, this.g, this.h, -1);
    }

    private boolean d(int i, int j, int k) {
        Block block = Block.byId[this.world.getTypeId(i, j, k)];
        int l = this.world.getData(i, j, k);
        if (block != null) {
            block.a(this.world, i, j, k, l, this.player);
        }

        boolean flag = this.world.setAir(i, j, k);
        if (block != null && flag) {
            block.postBreak(this.world, i, j, k, l);
        }

        return flag;
    }

    public boolean breakBlock(int i, int j, int k) {
        BlockBreakEvent event = null;
        if ((this.player != null)) {
            org.bukkit.block.Block block = this.world.getWorld().getBlockAt(i, j, k);
            // Freecam bug fix
            if (block != null && block.getState() instanceof InventoryHolder) {
                InventoryHolder holder = (InventoryHolder) block.getState();
                List<HumanEntity> heList = holder.getInventory().getViewers();
                if (heList != null && heList.size() > 0) {
                    return false;
                }
            }
            //
            if (this.world.getTileEntity(i, j, k) == null) {
                Packet53BlockChange packet = new Packet53BlockChange(i, j, k, this.world);

                packet.material = 0;
                packet.data = 0;
                this.player.playerConnection.sendPacket(packet);
            }
            event = new BlockBreakEvent(block, this.player.getBukkitEntity());

            event.setCancelled((this.gamemode.isAdventure()) && (!this.player.e(i, j, k)));

            Block nmsBlock = Block.byId[block.getTypeId()];
            if ((nmsBlock != null) && (!event.isCancelled()) && (!isCreative()) && (this.player.a(nmsBlock))) {
                if ((!nmsBlock.r_()) || (!EnchantmentManager.hasSilkTouchEnchantment(this.player))) {
                    int data = block.getData();
                    int bonusLevel = EnchantmentManager.getBonusBlockLootEnchantmentLevel(this.player);

                    event.setExpToDrop(nmsBlock.getExpDrop(this.world, data, bonusLevel));
                }
            }

            this.world.getServer().getPluginManager().callEvent(event);
            if (event.isCancelled()) {
                this.player.playerConnection.sendPacket(new Packet53BlockChange(i, j, k, this.world));

                TileEntity tileentity = this.world.getTileEntity(i, j, k);
                if (tileentity != null) {
                    this.player.playerConnection.sendPacket(tileentity.getUpdatePacket());
                }
                return false;
            }
        }
        int l = this.world.getTypeId(i, j, k);
        if (Block.byId[l] == null) {
            return false;
        }
        int i1 = this.world.getData(i, j, k);
        if ((l == Block.SKULL.id) && (!isCreative())) {
            Block.SKULL.dropNaturally(this.world, i, j, k, i1, 1.0F, 0);
            return d(i, j, k);
        }
        this.world.a(this.player, 2001, i, j, k, l + (this.world.getData(i, j, k) << 12));
        boolean flag = d(i, j, k);
        if (isCreative()) {
            this.player.playerConnection.sendPacket(new Packet53BlockChange(i, j, k, this.world));
        } else {
            ItemStack itemstack = this.player.cd();
            boolean flag1 = this.player.a(Block.byId[l]);
            if (itemstack != null) {
                itemstack.a(this.world, l, i, j, k, this.player);
                if (itemstack.count == 0) {
                    this.player.ce();
                }
            }
            if ((flag) && (flag1)) {
                Block.byId[l].a(this.world, this.player, i, j, k, i1);
            }
        }
        if ((flag) && (event != null)) {
            Block.byId[l].j(this.world, i, j, k, event.getExpToDrop());
        }
        return flag;
    }

    public boolean useItem(EntityHuman entityhuman, World world, ItemStack itemstack) {
        int i = itemstack.count;
        int j = itemstack.getData();
        ItemStack itemstack1 = itemstack.a(world, entityhuman);
        if (itemstack1 != itemstack || itemstack1 != null && (itemstack1.count != i || itemstack1.n() > 0 || itemstack1.getData() != j)) {
            entityhuman.inventory.items[entityhuman.inventory.itemInHandIndex] = itemstack1;
            if (this.isCreative()) {
                itemstack1.count = i;
                if (itemstack1.g()) {
                    itemstack1.setData(j);
                }
            }

            if (itemstack1.count == 0) {
                entityhuman.inventory.items[entityhuman.inventory.itemInHandIndex] = null;
            }

            if (!entityhuman.bX()) {
                ((EntityPlayer) entityhuman).updateInventory(entityhuman.defaultContainer);
            }

            return true;
        } else {
            return false;
        }
    }

    public boolean interact(EntityHuman entityhuman, World world, ItemStack itemstack, int i, int j, int k, int l, float f, float f1, float f2) {
        int i1 = world.getTypeId(i, j, k);
        boolean result = false;
        if (i1 > 0) {
            PlayerInteractEvent event = CraftEventFactory.callPlayerInteractEvent(entityhuman, Action.RIGHT_CLICK_BLOCK, i, j, k, l, itemstack);
            if (event.useInteractedBlock() == Result.DENY) {
                if (i1 == Block.WOODEN_DOOR.id) {
                    boolean j1 = (world.getData(i, j, k) & 8) == 0;
                    ((EntityPlayer) entityhuman).playerConnection.sendPacket(new Packet53BlockChange(i, j + (j1 ? 1 : -1), k, world));
                }

                result = event.useItemInHand() != Result.ALLOW;
            } else if (!entityhuman.isSneaking() || itemstack == null) {
                result = Block.byId[i1].interact(world, i, j, k, entityhuman, l, f, f1, f2);
            }

            if (itemstack != null && !result) {
                int j11 = itemstack.getData();
                int k1 = itemstack.count;
                result = itemstack.placeItem(entityhuman, world, i, j, k, l, f, f1, f2);
                if (this.isCreative()) {
                    itemstack.setData(j11);
                    itemstack.count = k1;
                }
            }

            if (itemstack != null && (!result && event.useItemInHand() != Result.DENY || event.useItemInHand() == Result.ALLOW)) {
                this.useItem(entityhuman, world, itemstack);
            }
        }

        return result;
    }

    public void a(WorldServer worldserver) {
        this.world = worldserver;
    }
}
