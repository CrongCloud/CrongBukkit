package org.bukkit.command;

/**
 * Created by EntryPoint on 2017-01-01.
 */

import org.apache.commons.lang.Validate;
import org.bukkit.Server;
import org.bukkit.command.defaults.*;
import org.bukkit.entity.Player;
import org.bukkit.util.Java15Compat;
import org.bukkit.util.StringUtil;

import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Pattern;

public class SimpleCommandMap implements CommandMap {
    protected static final Set<VanillaCommand> fallbackCommands = new HashSet();
    private static final Pattern PATTERN_ON_SPACE = Pattern.compile(" ", 16);

    static {
        fallbackCommands.add(new ListCommand());
        fallbackCommands.add(new OpCommand());
        fallbackCommands.add(new DeopCommand());
        fallbackCommands.add(new BanIpCommand());
        fallbackCommands.add(new PardonIpCommand());
        fallbackCommands.add(new BanCommand());
        fallbackCommands.add(new PardonCommand());
        fallbackCommands.add(new KickCommand());
        fallbackCommands.add(new TeleportCommand());
        fallbackCommands.add(new GiveCommand());
        fallbackCommands.add(new TimeCommand());
        fallbackCommands.add(new SayCommand());
        fallbackCommands.add(new WhitelistCommand());
        fallbackCommands.add(new TellCommand());
        fallbackCommands.add(new MeCommand());
        fallbackCommands.add(new KillCommand());
        fallbackCommands.add(new GameModeCommand());
        fallbackCommands.add(new HelpCommand());
        fallbackCommands.add(new ExpCommand());
        fallbackCommands.add(new ToggleDownfallCommand());
        fallbackCommands.add(new BanListCommand());
        fallbackCommands.add(new DefaultGameModeCommand());
        fallbackCommands.add(new SeedCommand());
        fallbackCommands.add(new DifficultyCommand());
        fallbackCommands.add(new WeatherCommand());
        fallbackCommands.add(new SpawnpointCommand());
        fallbackCommands.add(new ClearCommand());
        fallbackCommands.add(new GameRuleCommand());
        fallbackCommands.add(new EnchantCommand());
        fallbackCommands.add(new TestForCommand());
        fallbackCommands.add(new EffectCommand());
        fallbackCommands.add(new ScoreboardCommand());
    }

    protected final Map<String, Command> knownCommands = new HashMap();
    protected final Set<String> aliases = new HashSet();
    private final Server server;

    public SimpleCommandMap(Server server) {
        this.server = server;
        this.setDefaultCommands(server);
    }

    private void setDefaultCommands(Server server) {
        this.register("bukkit", new SaveCommand());
        this.register("bukkit", new SaveOnCommand());
        this.register("bukkit", new SaveOffCommand());
        this.register("bukkit", new StopCommand());
        this.register("bukkit", new VersionCommand("version"));
        this.register("bukkit", new ReloadCommand("reload"));
        this.register("bukkit", new PluginsCommand("plugins"));
        this.register("bukkit", new TimingsCommand("timings"));
        this.register("bukkit", new ReloadConfigCommand("configreload"));
    }

    public void registerAll(String fallbackPrefix, List<Command> commands) {
        if (commands != null) {
            Iterator i$ = commands.iterator();

            while (i$.hasNext()) {
                Command c = (Command) i$.next();
                this.register(fallbackPrefix, c);
            }
        }

    }

    public boolean register(String fallbackPrefix, Command command) {
        return this.register(command.getName(), fallbackPrefix, command);
    }

    public boolean register(String label, String fallbackPrefix, Command command) {
        boolean registeredPassedLabel = this.register(label, fallbackPrefix, command, false);
        Iterator iterator = command.getAliases().iterator();

        while (iterator.hasNext()) {
            if (!this.register((String) iterator.next(), fallbackPrefix, command, true)) {
                iterator.remove();
            }
        }

        command.register(this);
        return registeredPassedLabel;
    }

    private synchronized boolean register(String label, String fallbackPrefix, Command command, boolean isAlias) {
        String lowerLabel = label.trim().toLowerCase();
        if (isAlias && this.knownCommands.containsKey(lowerLabel)) {
            return false;
        } else {
            String lowerPrefix = fallbackPrefix.trim().toLowerCase();

            boolean registerdPassedLabel;
            for (registerdPassedLabel = true; this.knownCommands.containsKey(lowerLabel) && !this.aliases.contains(lowerLabel); registerdPassedLabel = false) {
                lowerLabel = lowerPrefix + ":" + lowerLabel;
            }

            if (isAlias) {
                this.aliases.add(lowerLabel);
            } else {
                this.aliases.remove(lowerLabel);
                command.setLabel(lowerLabel);
            }

            this.knownCommands.put(lowerLabel, command);
            return registerdPassedLabel;
        }
    }

    protected Command getFallback(String label) {
        Iterator i$ = fallbackCommands.iterator();

        VanillaCommand cmd;
        do {
            if (!i$.hasNext()) {
                return null;
            }

            cmd = (VanillaCommand) i$.next();
        } while (!cmd.matches(label));

        return cmd;
    }

    public Set<VanillaCommand> getFallbackCommands() {
        return Collections.unmodifiableSet(fallbackCommands);
    }

    public boolean dispatch(CommandSender sender, String commandLine) throws CommandException {
        String[] args = PATTERN_ON_SPACE.split(commandLine);
        if (args.length == 0) {
            return false;
        } else {
            String sentCommandLabel = args[0].toLowerCase();
            Command target = this.getCommand(sentCommandLabel);
            if (target == null) {
                return false;
            } else {
                try {
                    target.execute(sender, sentCommandLabel, Java15Compat.Arrays_copyOfRange(args, 1, args.length));
                    return true;
                } catch (CommandException var7) {
                    throw var7;
                } catch (Throwable var8) {
                    throw new CommandException("Unhandled exception executing \'" + commandLine + "\' in " + target, var8);
                }
            }
        }
    }

    public synchronized void clearCommands() {
        Iterator i$ = this.knownCommands.entrySet().iterator();

        while (i$.hasNext()) {
            Entry entry = (Entry) i$.next();
            ((Command) entry.getValue()).unregister(this);
        }

        this.knownCommands.clear();
        this.aliases.clear();
        this.setDefaultCommands(this.server);
    }

    public Command getCommand(String name) {
        Command target = this.knownCommands.get(name.toLowerCase());
        if (target == null) {
            target = this.getFallback(name);
        }

        return target;
    }

    public List<String> tabComplete(CommandSender sender, String cmdLine) {
        Validate.notNull(sender, "Sender cannot be null");
        Validate.notNull(cmdLine, "Command line cannot null");
        int spaceIndex = cmdLine.indexOf(32);
        String prefix = sender instanceof Player ? "/" : "";
        if (spaceIndex == -1) {
            ArrayList<String> completions = new ArrayList<>();
            Map target1 = this.knownCommands;
            Iterator argLine1 = fallbackCommands.iterator();

            while (argLine1.hasNext()) {
                VanillaCommand args1 = (VanillaCommand) argLine1.next();
                String ex = args1.getName();
                if (args1.testPermissionSilent(sender) && !target1.containsKey(ex) && StringUtil.startsWithIgnoreCase(ex, cmdLine)) {
                    completions.add(prefix + ex);
                }
            }

            argLine1 = target1.entrySet().iterator();

            while (argLine1.hasNext()) {
                Entry args2 = (Entry) argLine1.next();
                Command ex1 = (Command) args2.getValue();
                if (ex1.testPermissionSilent(sender)) {
                    String name = (String) args2.getKey();
                    if (StringUtil.startsWithIgnoreCase(name, cmdLine)) {
                        completions.add(prefix + name);
                    }
                }
            }

            Collections.sort(completions, String.CASE_INSENSITIVE_ORDER);
            return completions;
        } else {
            String commandName = cmdLine.substring(0, spaceIndex);
            Command target = this.getCommand(commandName);
            if (target == null) {
                return null;
            } else if (!target.testPermissionSilent(sender)) {
                return null;
            } else {
                String argLine = cmdLine.substring(spaceIndex + 1, cmdLine.length());
                String[] args = PATTERN_ON_SPACE.split(argLine, -1);

                try {
                    return target.tabComplete(sender, commandName, args);
                } catch (CommandException var10) {
                    throw var10;
                } catch (Throwable var11) {
                    throw new CommandException("Unhandled exception executing tab-completer for \'" + cmdLine + "\' in " + target, var11);
                }
            }
        }
    }

    public Collection<Command> getCommands() {
        return this.knownCommands.values();
    }

    public void registerServerAliases() {
        Map values = this.server.getCommandAliases();
        Iterator i$ = values.keySet().iterator();

        while (i$.hasNext()) {
            String alias = (String) i$.next();
            String[] targetNames = (String[]) values.get(alias);
            ArrayList targets = new ArrayList();
            StringBuilder bad = new StringBuilder();
            String[] arr$ = targetNames;
            int len$ = targetNames.length;

            for (int i$1 = 0; i$1 < len$; ++i$1) {
                String name = arr$[i$1];
                Command command = this.getCommand(name);
                if (command == null) {
                    if (bad.length() > 0) {
                        bad.append(", ");
                    }

                    bad.append(name);
                } else {
                    targets.add(command);
                }
            }

            if (targets.size() > 0) {
                this.knownCommands.put(alias.toLowerCase(), new MultipleCommandAlias(alias.toLowerCase(), (Command[]) targets.toArray(new Command[0])));
            } else {
                this.knownCommands.remove(alias.toLowerCase());
            }

            if (bad.length() > 0) {
                this.server.getLogger().warning("The following command(s) could not be aliased under \'" + alias + "\' because they do not exist: " + bad);
            }
        }

    }
}
