package org.bukkit.craftbukkit.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-29.
 */

import net.minecraft.server.v1_5_R3.BanEntry;
import net.minecraft.server.v1_5_R3.EntityPlayer;
import net.minecraft.server.v1_5_R3.NBTTagCompound;
import net.minecraft.server.v1_5_R3.WorldNBTStorage;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SerializableAs("Player")
public class CraftOfflinePlayer implements OfflinePlayer, ConfigurationSerializable {
    private final String name;
    private final CraftServer server;
    private final WorldNBTStorage storage;

    protected CraftOfflinePlayer(CraftServer server, String name) {
        this.server = server;
        this.name = name;
        this.storage = (WorldNBTStorage) server.console.worlds.get(0).getDataManager();
    }

    public static OfflinePlayer deserialize(Map<String, Object> args) {
        return Bukkit.getServer().getOfflinePlayer((String) args.get("name"));
    }

    public boolean isOnline() {
        return this.getPlayer() != null;
    }

    public String getName() {
        return this.name;
    }

    public Server getServer() {
        return this.server;
    }

    public boolean isOp() {
        return this.server.getHandle().isOp(this.getName().toLowerCase());
    }

    public void setOp(boolean value) {
        if (value != this.isOp()) {
            if (value) {
                this.server.getHandle().addOp(this.getName().toLowerCase());
            } else {
                this.server.getHandle().removeOp(this.getName().toLowerCase());
            }

        }
    }

    public boolean isBanned() {
        return this.server.getHandle().getNameBans().isBanned(this.name.toLowerCase());
    }

    public void setBanned(boolean value) {
        if (value) {
            BanEntry entry = new BanEntry(this.name.toLowerCase());
            this.server.getHandle().getNameBans().add(entry);
        } else {
            this.server.getHandle().getNameBans().remove(this.name.toLowerCase());
        }

        this.server.getHandle().getNameBans().save();
    }

    public boolean isWhitelisted() {
        return this.server.getHandle().getWhitelisted().contains(this.name.toLowerCase());
    }

    public void setWhitelisted(boolean value) {
        if (value) {
            this.server.getHandle().addWhitelist(this.name.toLowerCase());
        } else {
            this.server.getHandle().removeWhitelist(this.name.toLowerCase());
        }

    }

    public Map<String, Object> serialize() {
        LinkedHashMap result = new LinkedHashMap();
        result.put("name", this.name);
        return result;
    }

    public String toString() {
        return this.getClass().getSimpleName() + "[name=" + this.name + "]";
    }

    public Player getPlayer() {
        final EntityPlayer entityPlayer = server.getHandle().playerMap.get(getName());
        return entityPlayer != null ? entityPlayer.getBukkitEntity() : null;
        /*
        Iterator i$ = this.server.getHandle().players.iterator();

        EntityPlayer player;
        do {
            if(!i$.hasNext()) {
                return null;
            }

            Object obj = i$.next();
            player = (EntityPlayer)obj;
        } while(!player.name.equalsIgnoreCase(this.getName()));

        return player.playerConnection != null?player.playerConnection.getPlayer():null;
        */
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else if (!(obj instanceof OfflinePlayer)) {
            return false;
        } else {
            OfflinePlayer other = (OfflinePlayer) obj;
            return (this.getName() != null && other.getName() != null) && this.getName().equalsIgnoreCase(other.getName());
        }
    }

    public int hashCode() {
        byte hash = 5;
        int hash1 = 97 * hash + (this.getName() != null ? this.getName().toLowerCase().hashCode() : 0);
        return hash1;
    }

    private NBTTagCompound getData() {
        return this.storage.getPlayerData(this.getName());
    }

    private NBTTagCompound getBukkitData() {
        NBTTagCompound result = this.getData();
        if (result != null) {
            if (!result.hasKey("bukkit")) {
                result.setCompound("bukkit", new NBTTagCompound());
            }

            result = result.getCompound("bukkit");
        }

        return result;
    }

    private File getDataFile() {
        return new File(this.storage.getPlayerDir(), this.name + ".dat");
    }

    public long getFirstPlayed() {
        Player player = this.getPlayer();
        if (player != null) {
            return player.getFirstPlayed();
        } else {
            NBTTagCompound data = this.getBukkitData();
            if (data != null) {
                if (data.hasKey("firstPlayed")) {
                    return data.getLong("firstPlayed");
                } else {
                    File file = this.getDataFile();
                    return file.lastModified();
                }
            } else {
                return 0L;
            }
        }
    }

    public long getLastPlayed() {
        Player player = this.getPlayer();
        if (player != null) {
            return player.getLastPlayed();
        } else {
            NBTTagCompound data = this.getBukkitData();
            if (data != null) {
                if (data.hasKey("lastPlayed")) {
                    return data.getLong("lastPlayed");
                } else {
                    File file = this.getDataFile();
                    return file.lastModified();
                }
            } else {
                return 0L;
            }
        }
    }

    public boolean hasPlayedBefore() {
        return this.getData() != null;
    }

    public Location getBedSpawnLocation() {
        NBTTagCompound data = this.getData();
        if (data == null) {
            return null;
        } else if (data.hasKey("SpawnX") && data.hasKey("SpawnY") && data.hasKey("SpawnZ")) {
            String spawnWorld = data.getString("SpawnWorld");
            if (spawnWorld.equals("")) {
                spawnWorld = this.server.getWorlds().get(0).getName();
            }

            return new Location(this.server.getWorld(spawnWorld), (double) data.getInt("SpawnX"), (double) data.getInt("SpawnY"), (double) data.getInt("SpawnZ"));
        } else {
            return null;
        }
    }

    public void setMetadata(String metadataKey, MetadataValue metadataValue) {
        this.server.getPlayerMetadata().setMetadata(this, metadataKey, metadataValue);
    }

    public List<MetadataValue> getMetadata(String metadataKey) {
        return this.server.getPlayerMetadata().getMetadata(this, metadataKey);
    }

    public boolean hasMetadata(String metadataKey) {
        return this.server.getPlayerMetadata().hasMetadata(this, metadataKey);
    }

    public void removeMetadata(String metadataKey, Plugin plugin) {
        this.server.getPlayerMetadata().removeMetadata(this, metadataKey, plugin);
    }
}

