package org.bukkit.craftbukkit.v1_5_R3.inventory;

/**
 * Created by EntryPoint on 2016-12-30.
 */

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap.Builder;
import net.minecraft.server.v1_5_R3.NBTTagCompound;
import net.minecraft.server.v1_5_R3.NBTTagList;
import net.minecraft.server.v1_5_R3.NBTTagString;
import net.minecraft.server.v1_5_R3.SwiftUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.DelegateDeserialization;
import org.bukkit.craftbukkit.v1_5_R3.inventory.CraftMetaItem.SerializableMeta;
import org.bukkit.inventory.meta.BookMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@DelegateDeserialization(SerializableMeta.class)
class CraftMetaBook extends CraftMetaItem implements BookMeta {
    static final ItemMetaKey BOOK_TITLE = new ItemMetaKey("title");
    static final ItemMetaKey BOOK_AUTHOR = new ItemMetaKey("author");
    static final ItemMetaKey BOOK_PAGES = new ItemMetaKey("pages");
    static final int MAX_PAGE_LENGTH = 256;
    static final int MAX_TITLE_LENGTH = 65535;
    private String title;
    private String author;
    private List<String> pages = new ArrayList();

    CraftMetaBook(CraftMetaItem meta) {
        super(meta);
        if (meta instanceof CraftMetaBook) {
            CraftMetaBook bookMeta = (CraftMetaBook) meta;
            this.title = bookMeta.title;
            this.author = bookMeta.author;
            this.pages.addAll(bookMeta.pages);
        }
    }

    CraftMetaBook(NBTTagCompound tag) {
        super(tag);
        if (tag.hasKey(BOOK_TITLE.NBT)) {
            this.title = SwiftUtils.limit(tag.getString(BOOK_TITLE.NBT), 1024);
        }

        if (tag.hasKey(BOOK_AUTHOR.NBT)) {
            this.author = SwiftUtils.limit(tag.getString(BOOK_AUTHOR.NBT), 1024);
        }

        if (tag.hasKey(BOOK_PAGES.NBT)) {
            NBTTagList pages = tag.getList(BOOK_PAGES.NBT);
            String[] pageArray = new String[pages.size()];

            for (int i = 0; i < pages.size(); ++i) {
                String page = SwiftUtils.limit(((NBTTagString) pages.get(i)).data, 2048);
                pageArray[i] = page;
            }

            this.addPage(pageArray);
        }

    }

    CraftMetaBook(Map<String, Object> map) {
        super(map);
        this.setAuthor(SerializableMeta.getString(map, BOOK_AUTHOR.BUKKIT, true));
        this.setTitle(SerializableMeta.getString(map, BOOK_TITLE.BUKKIT, true));
        Iterable pages = SerializableMeta.getObject(Iterable.class, map, BOOK_PAGES.BUKKIT, true);
        CraftMetaItem.safelyAdd(pages, this.pages, 256);
    }

    void applyToItem(NBTTagCompound itemData) {
        super.applyToItem(itemData);
        if (this.hasTitle()) {
            itemData.setString(BOOK_TITLE.NBT, this.title);
        }

        if (this.hasAuthor()) {
            itemData.setString(BOOK_AUTHOR.NBT, this.author);
        }

        if (this.hasPages()) {
            itemData.set(BOOK_PAGES.NBT, createStringList(this.pages, BOOK_PAGES));
        }

    }

    boolean isEmpty() {
        return super.isEmpty() && this.isBookEmpty();
    }

    boolean isBookEmpty() {
        return !this.hasPages() && !this.hasAuthor() && !this.hasTitle();
    }

    boolean applicableTo(Material type) {
        switch (type) {
            case WRITTEN_BOOK:
            case BOOK_AND_QUILL:
                return true;
        }
        return false;
    }

    public boolean hasAuthor() {
        return !Strings.isNullOrEmpty(this.author);
    }

    public boolean hasTitle() {
        return !Strings.isNullOrEmpty(this.title);
    }

    public boolean hasPages() {
        return !this.pages.isEmpty();
    }

    public String getTitle() {
        return this.title;
    }

    public boolean setTitle(String title) {
        if (title == null) {
            this.title = null;
            return true;
        } else if (title.length() > '\uffff') {
            return false;
        } else {
            this.title = title;
            return true;
        }
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPage(int page) {
        Validate.isTrue(this.isValidPage(page), "Invalid page number");
        return this.pages.get(page - 1);
    }

    public void setPage(int page, String text) {
        if (!this.isValidPage(page)) {
            throw new IllegalArgumentException("Invalid page number " + page + "/" + this.pages.size());
        } else {
            this.pages.set(page - 1, text == null ? "" : (text.length() > 256 ? text.substring(0, 256) : text));
        }
    }

    public void setPages(String... pages) {
        this.pages.clear();
        this.addPage(pages);
    }

    public void addPage(String... pages) {
        String[] arr$ = pages;
        int len$ = pages.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            String page = arr$[i$];
            if (page == null) {
                page = "";
            } else if (page.length() > 256) {
                page = page.substring(0, 256);
            }

            this.pages.add(page);
        }

    }

    public int getPageCount() {
        return this.pages.size();
    }

    public List<String> getPages() {
        return ImmutableList.copyOf(this.pages);
    }

    public void setPages(List<String> pages) {
        this.pages.clear();
        CraftMetaItem.safelyAdd(pages, this.pages, 256);
    }

    private boolean isValidPage(int page) {
        return page > 0 && page <= this.pages.size();
    }

    public CraftMetaBook clone() {
        CraftMetaBook meta = (CraftMetaBook) super.clone();
        meta.pages = new ArrayList(this.pages);
        return meta;
    }

    int applyHash() {
        int original;
        int hash = original = super.applyHash();
        if (this.hasTitle()) {
            hash = 61 * hash + this.title.hashCode();
        }

        if (this.hasAuthor()) {
            hash = 61 * hash + 13 * this.author.hashCode();
        }

        if (this.hasPages()) {
            hash = 61 * hash + 17 * this.pages.hashCode();
        }

        return original != hash ? CraftMetaBook.class.hashCode() ^ hash : hash;
    }

    boolean equalsCommon(CraftMetaItem meta) {
        if (!super.equalsCommon(meta)) {
            return false;
        } else if (!(meta instanceof CraftMetaBook)) {
            return true;
        } else {
            boolean var10000;
            label60:
            {
                label53:
                {
                    CraftMetaBook that = (CraftMetaBook) meta;
                    if (this.hasTitle()) {
                        if (!that.hasTitle() || !this.title.equals(that.title)) {
                            break label53;
                        }
                    } else if (that.hasTitle()) {
                        break label53;
                    }

                    if (this.hasAuthor()) {
                        if (!that.hasAuthor() || !this.author.equals(that.author)) {
                            break label53;
                        }
                    } else if (that.hasAuthor()) {
                        break label53;
                    }

                    if (this.hasPages()) {
                        if (that.hasPages() && this.pages.equals(that.pages)) {
                            break label60;
                        }
                    } else if (!that.hasPages()) {
                        break label60;
                    }
                }

                var10000 = false;
                return var10000;
            }

            var10000 = true;
            return var10000;
        }
    }

    boolean notUncommon(CraftMetaItem meta) {
        return super.notUncommon(meta) && (meta instanceof CraftMetaBook || this.isBookEmpty());
    }

    Builder<String, Object> serialize(Builder<String, Object> builder) {
        super.serialize(builder);
        if (this.hasTitle()) {
            builder.put(BOOK_TITLE.BUKKIT, this.title);
        }

        if (this.hasAuthor()) {
            builder.put(BOOK_AUTHOR.BUKKIT, this.author);
        }

        if (this.hasPages()) {
            builder.put(BOOK_PAGES.BUKKIT, this.pages);
        }

        return builder;
    }
}
