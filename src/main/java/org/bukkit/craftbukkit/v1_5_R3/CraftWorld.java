package org.bukkit.craftbukkit.v1_5_R3;

/**
 * Created by EntryPoint on 2016-12-31.
 */

import net.minecraft.server.v1_5_R3.*;
import net.minecraft.server.v1_5_R3.Material;
import org.apache.commons.lang.Validate;
import org.bukkit.*;
import org.bukkit.Chunk;
import org.bukkit.Effect.Type;
import org.bukkit.WorldType;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_5_R3.block.CraftBlock;
import org.bukkit.craftbukkit.v1_5_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_5_R3.entity.CraftItem;
import org.bukkit.craftbukkit.v1_5_R3.entity.CraftLightningStrike;
import org.bukkit.craftbukkit.v1_5_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_5_R3.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_5_R3.metadata.BlockMetadataStore;
import org.bukkit.craftbukkit.v1_5_R3.util.LongHash;
import org.bukkit.entity.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.minecart.*;
import org.bukkit.entity.minecart.PoweredMinecart;
import org.bukkit.entity.minecart.StorageMinecart;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.event.world.SpawnChangeEvent;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.messaging.StandardMessenger;
import org.bukkit.util.Vector;
import org.spigotmc.SpigotConfig;

import java.io.File;
import java.util.*;

import static org.bukkit.block.BlockFace.*;

public class CraftWorld implements org.bukkit.World {
    public static final int CUSTOM_DIMENSION_OFFSET = 10;
    private static final Random rand = new Random();
    private final WorldServer world;
    private final CraftServer server = (CraftServer) Bukkit.getServer();
    private final ChunkGenerator generator;
    private final List<BlockPopulator> populators = new ArrayList();
    private final BlockMetadataStore blockMetadata = new BlockMetadataStore(this);
    private final Spigot spigot = new Spigot() {
        public void playEffect(Location location, Effect effect, int id, int data, float offsetX, float offsetY, float offsetZ, float speed, int particleCount, int radius) {
            Validate.notNull(location, "Location cannot be null");
            Validate.notNull(effect, "Effect cannot be null");
            Validate.notNull(location.getWorld(), "World cannot be null");
            int distance;
            Object packet;
            if (effect.getType() != Type.PARTICLE) {
                distance = effect.getId();
                packet = new Packet61WorldEvent(distance, location.getBlockX(), location.getBlockY(), location.getBlockZ(), id, false);
            } else {
                StringBuilder distance1 = new StringBuilder();
                distance1.append(effect.getName());
                if (effect.getData() != null && (effect.getData().equals(Material.class) || effect.getData().equals(MaterialData.class))) {
                    distance1.append('_').append(id);
                }

                if (effect.getData() != null && effect.getData().equals(MaterialData.class)) {
                    distance1.append('_').append(data);
                }

                packet = new Packet63WorldParticles(distance1.toString(), (float) location.getX(), (float) location.getY(), (float) location.getZ(), offsetX, offsetY, offsetZ, speed, particleCount);
            }

            radius *= radius;
            Iterator i$ = CraftWorld.this.getPlayers().iterator();

            while (i$.hasNext()) {
                Player player = (Player) i$.next();
                if (((CraftPlayer) player).getHandle().playerConnection != null && location.getWorld().equals(player.getWorld())) {
                    distance = (int) player.getLocation().distanceSquared(location);
                    if (distance <= radius) {
                        ((CraftPlayer) player).getHandle().playerConnection.sendPacket((Packet) packet);
                    }
                }
            }

        }

        public void playEffect(Location location, Effect effect) {
            CraftWorld.this.playEffect(location, effect, 0);
        }
    };
    private Environment environment;
    private int monsterSpawn = -1;
    private int animalSpawn = -1;
    private int waterAnimalSpawn = -1;
    private int ambientSpawn = -1;
    private int chunkLoadCount = 0;
    private int chunkGCTickCount;

    public CraftWorld(WorldServer world, ChunkGenerator gen, Environment env) {
        this.world = world;
        this.generator = gen;
        this.environment = env;
        if (this.server.chunkGCPeriod > 0) {
            this.chunkGCTickCount = rand.nextInt(this.server.chunkGCPeriod);
        }

    }

    public Block getBlockAt(int x, int y, int z) {
        return this.getChunkAt(x >> 4, z >> 4).getBlock(x & 15, y & 255, z & 15);
    }

    public int getBlockTypeIdAt(int x, int y, int z) {
        return this.world.getTypeId(x, y, z);
    }

    public int getHighestBlockYAt(int x, int z) {
        if (!this.isChunkLoaded(x >> 4, z >> 4)) {
            this.loadChunk(x >> 4, z >> 4);
        }

        return this.world.getHighestBlockYAt(x, z);
    }

    public Location getSpawnLocation() {
        ChunkCoordinates spawn = this.world.getSpawn();
        return new Location(this, (double) spawn.x, (double) spawn.y, (double) spawn.z);
    }

    public boolean setSpawnLocation(int x, int y, int z) {
        try {
            Location e = this.getSpawnLocation();
            this.world.worldData.setSpawn(x, y, z);
            SpawnChangeEvent event = new SpawnChangeEvent(this, e);
            this.server.getPluginManager().callEvent(event);
            return true;
        } catch (Exception var6) {
            return false;
        }
    }

    public Chunk getChunkAt(int x, int z) {
        return this.world.chunkProviderServer.getChunkAt(x, z).bukkitChunk;
    }

    public Chunk getChunkAt(Block block) {
        return this.getChunkAt(block.getX() >> 4, block.getZ() >> 4);
    }

    public boolean isChunkLoaded(int x, int z) {
        return this.world.chunkProviderServer.isChunkLoaded(x, z);
    }

    public org.bukkit.Chunk[] getLoadedChunks() {
        Object[] chunks = this.world.chunkProviderServer.chunks.values().toArray();
        org.bukkit.Chunk[] craftChunks = new CraftChunk[chunks.length];
        for (int i = 0; i < chunks.length; i++) {
            net.minecraft.server.v1_5_R3.Chunk chunk = (net.minecraft.server.v1_5_R3.Chunk) chunks[i];
            craftChunks[i] = chunk.bukkitChunk;
        }
        return craftChunks;
    }

    public void loadChunk(int x, int z) {
        this.loadChunk(x, z, true);
    }

    public boolean unloadChunk(Chunk chunk) {
        return this.unloadChunk(chunk.getX(), chunk.getZ());
    }

    public boolean unloadChunk(int x, int z) {
        return this.unloadChunk(x, z, true);
    }

    public boolean unloadChunk(int x, int z, boolean save) {
        return this.unloadChunk(x, z, save, false);
    }

    public boolean unloadChunkRequest(int x, int z) {
        return this.unloadChunkRequest(x, z, true);
    }

    public boolean unloadChunkRequest(int x, int z, boolean safe) {
        if (Thread.currentThread() != MinecraftServer.getServer().primaryThread) {
            throw new IllegalStateException("Asynchronous chunk unload!");
        } else if (safe && this.isChunkInUse(x, z)) {
            return false;
        } else {
            this.world.chunkProviderServer.queueUnload(x, z);
            return true;
        }
    }

    public boolean unloadChunk(int x, int z, boolean save, boolean safe) {
        if (Thread.currentThread() != MinecraftServer.getServer().primaryThread) {
            throw new IllegalStateException("Asynchronous chunk unload!");
        } else if (safe && this.isChunkInUse(x, z)) {
            return false;
        } else {
            net.minecraft.server.v1_5_R3.Chunk chunk = this.world.chunkProviderServer.getOrCreateChunk(x, z);
            if (chunk.mustSave) {
                save = true;
            }

            chunk.removeEntities();
            if (save && !(chunk instanceof EmptyChunk)) {
                this.world.chunkProviderServer.saveChunk(chunk);
                this.world.chunkProviderServer.saveChunkNOP(chunk);
            }

            this.world.chunkProviderServer.unloadQueue.remove(x, z);
            this.world.chunkProviderServer.chunks.remove(LongHash.toLong(x, z));
            return true;
        }
    }

    public boolean regenerateChunk(int x, int z) {
        this.unloadChunk(x, z, false, false);
        this.world.chunkProviderServer.unloadQueue.remove(x, z);
        net.minecraft.server.v1_5_R3.Chunk chunk = null;
        if (this.world.chunkProviderServer.chunkProvider == null) {
            chunk = this.world.chunkProviderServer.emptyChunk;
        } else {
            chunk = this.world.chunkProviderServer.chunkProvider.getOrCreateChunk(x, z);
        }

        this.chunkLoadPostProcess(chunk, x, z);
        this.refreshChunk(x, z);
        return chunk != null;
    }

    public boolean refreshChunk(int x, int z) {
        if (!this.isChunkLoaded(x, z)) {
            return false;
        } else {
            int px = x << 4;
            int pz = z << 4;
            int height = this.getMaxHeight() / 16;

            for (int idx = 0; idx < 64; ++idx) {
                this.world.notify(px + idx / height, idx % height * 16, pz);
            }

            this.world.notify(px + 15, height * 16 - 1, pz + 15);
            return true;
        }
    }

    public boolean isChunkInUse(int x, int z) {
        return this.world.getPlayerChunkMap().isChunkInUse(x, z);
    }

    public boolean loadChunk(int x, int z, boolean generate) {
        if (Thread.currentThread() != MinecraftServer.getServer().primaryThread) {
            throw new IllegalStateException("Asynchronous chunk load!");
        } else {
            ++this.chunkLoadCount;
            if (generate) {
                return this.world.chunkProviderServer.getChunkAt(x, z) != null;
            } else {
                this.world.chunkProviderServer.unloadQueue.remove(x, z);
                net.minecraft.server.v1_5_R3.Chunk chunk = this.world.chunkProviderServer.chunks.get(LongHash.toLong(x, z));
                if (chunk == null) {
                    chunk = this.world.chunkProviderServer.loadChunk(x, z);
                    this.chunkLoadPostProcess(chunk, x, z);
                }

                return chunk != null;
            }
        }
    }

    private void chunkLoadPostProcess(net.minecraft.server.v1_5_R3.Chunk chunk, int x, int z) {
        if (chunk != null) {
            this.world.chunkProviderServer.chunks.put(LongHash.toLong(x, z), chunk);
            chunk.addEntities();
            if (!chunk.done && this.world.chunkProviderServer.isChunkLoaded(x + 1, z + 1) && this.world.chunkProviderServer.isChunkLoaded(x, z + 1) && this.world.chunkProviderServer.isChunkLoaded(x + 1, z)) {
                this.world.chunkProviderServer.getChunkAt(this.world.chunkProviderServer, x, z);
            }

            if (this.world.chunkProviderServer.isChunkLoaded(x - 1, z) && !this.world.chunkProviderServer.getOrCreateChunk(x - 1, z).done && this.world.chunkProviderServer.isChunkLoaded(x - 1, z + 1) && this.world.chunkProviderServer.isChunkLoaded(x, z + 1) && this.world.chunkProviderServer.isChunkLoaded(x - 1, z)) {
                this.world.chunkProviderServer.getChunkAt(this.world.chunkProviderServer, x - 1, z);
            }

            if (this.world.chunkProviderServer.isChunkLoaded(x, z - 1) && !this.world.chunkProviderServer.getOrCreateChunk(x, z - 1).done && this.world.chunkProviderServer.isChunkLoaded(x + 1, z - 1) && this.world.chunkProviderServer.isChunkLoaded(x, z - 1) && this.world.chunkProviderServer.isChunkLoaded(x + 1, z)) {
                this.world.chunkProviderServer.getChunkAt(this.world.chunkProviderServer, x, z - 1);
            }

            if (this.world.chunkProviderServer.isChunkLoaded(x - 1, z - 1) && !this.world.chunkProviderServer.getOrCreateChunk(x - 1, z - 1).done && this.world.chunkProviderServer.isChunkLoaded(x - 1, z - 1) && this.world.chunkProviderServer.isChunkLoaded(x, z - 1) && this.world.chunkProviderServer.isChunkLoaded(x - 1, z)) {
                this.world.chunkProviderServer.getChunkAt(this.world.chunkProviderServer, x - 1, z - 1);
            }
        }

    }

    public boolean isChunkLoaded(Chunk chunk) {
        return this.isChunkLoaded(chunk.getX(), chunk.getZ());
    }

    public void loadChunk(Chunk chunk) {
        this.loadChunk(chunk.getX(), chunk.getZ());
        ((CraftChunk) this.getChunkAt(chunk.getX(), chunk.getZ())).getHandle().bukkitChunk = chunk;
    }

    public WorldServer getHandle() {
        return this.world;
    }

    public Item dropItem(Location loc, ItemStack item) {
        Validate.notNull(item, "Cannot drop a Null item.");
        Validate.isTrue(item.getTypeId() != 0, "Cannot drop AIR.");
        EntityItem entity = new EntityItem(this.world, loc.getX(), loc.getY(), loc.getZ(), CraftItemStack.asNMSCopy(item));
        entity.pickupDelay = 10;
        this.world.addEntity(entity);
        return new CraftItem(this.world.getServer(), entity);
    }

    public Item dropItemNaturally(Location loc, ItemStack item) {
        loc = loc.clone();
        if (SpigotConfig.onlyOnceCalculateWhenDrop) {
            double one = world.random.nextFloat() * 0.7F + 0.3F * 0.5D;
            loc.setX(loc.getX() + one);
            loc.setY(loc.getY() + one);
            loc.setZ(loc.getZ() + one);
        } else {
            double xs = (double) (this.world.random.nextFloat() * 0.7F) + 0.15000000596046448D;
            double ys = (double) (this.world.random.nextFloat() * 0.7F) + 0.15000000596046448D;
            double zs = (double) (this.world.random.nextFloat() * 0.7F) + 0.15000000596046448D;
            loc.setX(loc.getX() + xs);
            loc.setY(loc.getY() + ys);
            loc.setZ(loc.getZ() + zs);
        }
        return this.dropItem(loc, item);
    }

    public Arrow spawnArrow(Location loc, Vector velocity, float speed, float spread) {
        Validate.notNull(loc, "Can not spawn arrow with a null location");
        Validate.notNull(velocity, "Can not spawn arrow with a null velocity");
        EntityArrow arrow = new EntityArrow(this.world);
        arrow.setPositionRotation(loc.getX(), loc.getY(), loc.getZ(), loc.getPitch(), loc.getYaw());
        arrow.shoot(velocity.getX(), velocity.getY(), velocity.getZ(), speed, spread);
        this.world.addEntity(arrow);
        return (Arrow) arrow.getBukkitEntity();
    }

    /**
     * @deprecated
     */
    @Deprecated
    public LivingEntity spawnCreature(Location loc, CreatureType creatureType) {
        return this.spawnCreature(loc, creatureType.toEntityType());
    }

    /**
     * @deprecated
     */
    @Deprecated
    public LivingEntity spawnCreature(Location loc, EntityType creatureType) {
        Validate.isTrue(creatureType.isAlive(), "EntityType not instance of LivingEntity");
        return (LivingEntity) this.spawnEntity(loc, creatureType);
    }

    public Entity spawnEntity(Location loc, EntityType entityType) {
        return this.spawn(loc, entityType.getEntityClass());
    }

    public LightningStrike strikeLightning(Location loc) {
        EntityLightning lightning = new EntityLightning(this.world, loc.getX(), loc.getY(), loc.getZ());
        this.world.strikeLightning(lightning);
        return new CraftLightningStrike(this.server, lightning);
    }

    public LightningStrike strikeLightningEffect(Location loc) {
        EntityLightning lightning = new EntityLightning(this.world, loc.getX(), loc.getY(), loc.getZ(), true);
        this.world.strikeLightning(lightning);
        return new CraftLightningStrike(this.server, lightning);
    }

    public boolean generateTree(Location loc, TreeType type) {
        return this.generateTree(loc, type, this.world);
    }

    public boolean generateTree(Location loc, TreeType type, BlockChangeDelegate delegate) {
        BlockSapling.TreeGenerator gen;
        switch (type) {
            case BIG_TREE:
                gen = new WorldGenBigTree(true);
                break;
            case BIRCH:
                gen = new WorldGenForest(true);
                break;
            case REDWOOD:
                gen = new WorldGenTaiga2(true);
                break;
            case TALL_REDWOOD:
                gen = new WorldGenTaiga1();
                break;
            case JUNGLE:
                gen = new WorldGenMegaTree(true, 10 + rand.nextInt(20), 3, 3);
                break;
            case SMALL_JUNGLE:
                gen = new WorldGenTrees(true, 4 + rand.nextInt(7), 3, 3, false);
                break;
            case JUNGLE_BUSH:
                gen = new WorldGenGroundBush(3, 0);
                break;
            case RED_MUSHROOM:
                gen = new WorldGenHugeMushroom(1);
                break;
            case BROWN_MUSHROOM:
                gen = new WorldGenHugeMushroom(0);
                break;
            case SWAMP:
                gen = new WorldGenSwampTree();
                break;
            case TREE:
            default:
                gen = new WorldGenTrees(true);
                break;
        }

        return gen.generate(delegate, rand, loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
    }

    public TileEntity getTileEntityAt(int x, int y, int z) {
        return this.world.getTileEntity(x, y, z);
    }

    public String getName() {
        return this.world.worldData.getName();
    }

    /**
     * @deprecated
     */
    @Deprecated
    public long getId() {
        return this.world.worldData.getSeed();
    }

    public UUID getUID() {
        return this.world.getDataManager().getUUID();
    }

    public String toString() {
        return "CraftWorld{name=" + this.getName() + '}';
    }

    public long getTime() {
        long time = this.getFullTime() % 24000L;
        if (time < 0L) {
            time += 24000L;
        }

        return time;
    }

    public void setTime(long time) {
        long margin = (time - this.getFullTime()) % 24000L;
        if (margin < 0L) {
            margin += 24000L;
        }

        this.setFullTime(this.getFullTime() + margin);
    }

    public long getFullTime() {
        return this.world.getDayTime();
    }

    public void setFullTime(long time) {
        this.world.setDayTime(time);
        Iterator i$ = this.getPlayers().iterator();

        while (i$.hasNext()) {
            Player p = (Player) i$.next();
            CraftPlayer cp = (CraftPlayer) p;
            if (cp.getHandle().playerConnection != null) {
                cp.getHandle().playerConnection.sendPacket(new Packet4UpdateTime(cp.getHandle().world.getTime(), cp.getHandle().getPlayerTime()));
            }
        }

    }

    public boolean createExplosion(double x, double y, double z, float power) {
        return this.createExplosion(x, y, z, power, false, true);
    }

    public boolean createExplosion(double x, double y, double z, float power, boolean setFire) {
        return this.createExplosion(x, y, z, power, setFire, true);
    }

    public boolean createExplosion(double x, double y, double z, float power, boolean setFire, boolean breakBlocks) {
        return !this.world.createExplosion(null, x, y, z, power, setFire, breakBlocks).wasCanceled;
    }

    public boolean createExplosion(Location loc, float power) {
        return this.createExplosion(loc, power, false);
    }

    public boolean createExplosion(Location loc, float power, boolean setFire) {
        return this.createExplosion(loc.getX(), loc.getY(), loc.getZ(), power, setFire);
    }

    public Environment getEnvironment() {
        return this.environment;
    }

    public void setEnvironment(Environment env) {
        if (this.environment != env) {
            this.environment = env;
            this.world.worldProvider = WorldProvider.byDimension(this.environment.getId());
        }

    }

    public Block getBlockAt(Location location) {
        return this.getBlockAt(location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    public int getBlockTypeIdAt(Location location) {
        return this.getBlockTypeIdAt(location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }

    public int getHighestBlockYAt(Location location) {
        return this.getHighestBlockYAt(location.getBlockX(), location.getBlockZ());
    }

    public Chunk getChunkAt(Location location) {
        return this.getChunkAt(location.getBlockX() >> 4, location.getBlockZ() >> 4);
    }

    public ChunkGenerator getGenerator() {
        return this.generator;
    }

    public List<BlockPopulator> getPopulators() {
        return this.populators;
    }

    public Block getHighestBlockAt(int x, int z) {
        return this.getBlockAt(x, this.getHighestBlockYAt(x, z), z);
    }

    public Block getHighestBlockAt(Location location) {
        return this.getHighestBlockAt(location.getBlockX(), location.getBlockZ());
    }

    public Biome getBiome(int x, int z) {
        return CraftBlock.biomeBaseToBiome(this.world.getBiome(x, z));
    }

    public void setBiome(int x, int z, Biome bio) {
        BiomeBase bb = CraftBlock.biomeToBiomeBase(bio);
        if (this.world.isLoaded(x, 0, z)) {
            net.minecraft.server.v1_5_R3.Chunk chunk = this.world.getChunkAtWorldCoords(x, z);
            if (chunk != null) {
                byte[] biomevals = chunk.m();
                biomevals[(z & 15) << 4 | x & 15] = (byte) bb.id;
            }
        }

    }

    public double getTemperature(int x, int z) {
        return (double) this.world.getBiome(x, z).temperature;
    }

    public double getHumidity(int x, int z) {
        return (double) this.world.getBiome(x, z).humidity;
    }

    public List<Entity> getEntities() {
        ArrayList list = new ArrayList();
        Iterator i$ = this.world.entityList.iterator();

        while (i$.hasNext()) {
            Object o = i$.next();
            if (o instanceof net.minecraft.server.v1_5_R3.Entity) {
                net.minecraft.server.v1_5_R3.Entity mcEnt = (net.minecraft.server.v1_5_R3.Entity) o;
                CraftEntity bukkitEntity = mcEnt.getBukkitEntity();
                if (bukkitEntity != null) {
                    list.add(bukkitEntity);
                }
            }
        }

        return list;
    }

    public List<LivingEntity> getLivingEntities() {
        ArrayList list = new ArrayList();
        Iterator i$ = this.world.entityList.iterator();

        while (i$.hasNext()) {
            Object o = i$.next();
            if (o instanceof net.minecraft.server.v1_5_R3.Entity) {
                net.minecraft.server.v1_5_R3.Entity mcEnt = (net.minecraft.server.v1_5_R3.Entity) o;
                CraftEntity bukkitEntity = mcEnt.getBukkitEntity();
                if (bukkitEntity != null && bukkitEntity instanceof LivingEntity) {
                    list.add(bukkitEntity);
                }
            }
        }

        return list;
    }

    /**
     * @deprecated
     */
    @Deprecated
    @SuppressWarnings("unchecked")
    public <T extends Entity> Collection<T> getEntitiesByClass(Class<T>... classes) {
        return (Collection<T>) getEntitiesByClasses(classes);
    }

    public <T extends org.bukkit.entity.Entity> Collection<T> getEntitiesByClass(Class<T> clazz) {
        Collection<T> list = new ArrayList<>();
        for (Object entity : this.world.entityList) {
            if ((entity instanceof net.minecraft.server.v1_5_R3.Entity)) {
                org.bukkit.entity.Entity bukkitEntity = ((net.minecraft.server.v1_5_R3.Entity) entity).getBukkitEntity();
                if (bukkitEntity != null) {
                    Class<?> bukkitClass = bukkitEntity.getClass();
                    if (clazz.isAssignableFrom(bukkitClass)) {
                        list.add((T) bukkitEntity);
                    }
                }
            }
        }
        return list;
    }

    public Collection<org.bukkit.entity.Entity> getEntitiesByClasses(Class... classes) {
        Collection<org.bukkit.entity.Entity> list = new ArrayList<>();
        for (Object entity : this.world.entityList) {
            if ((entity instanceof net.minecraft.server.v1_5_R3.Entity)) {
                org.bukkit.entity.Entity bukkitEntity = ((net.minecraft.server.v1_5_R3.Entity) entity).getBukkitEntity();
                if (bukkitEntity != null) {
                    Class<?> bukkitClass = bukkitEntity.getClass();
                    for (Class<?> clazz : classes) {
                        if (clazz.isAssignableFrom(bukkitClass)) {
                            list.add(bukkitEntity);
                            break;
                        }
                    }
                }
            }
        }
        return list;
    }

    public List<Player> getPlayers() {
        ArrayList list = new ArrayList();
        Iterator i$ = this.world.entityList.iterator();

        while (i$.hasNext()) {
            Object o = i$.next();
            if (o instanceof net.minecraft.server.v1_5_R3.Entity) {
                net.minecraft.server.v1_5_R3.Entity mcEnt = (net.minecraft.server.v1_5_R3.Entity) o;
                CraftEntity bukkitEntity = mcEnt.getBukkitEntity();
                if (bukkitEntity != null && bukkitEntity instanceof Player) {
                    list.add(bukkitEntity);
                }
            }
        }

        return list;
    }

    public void save() {
        save(true);
    }

    public void save(boolean forceSave) {
        try {
            boolean ex = this.world.savingDisabled;
            this.world.savingDisabled = false;
            this.world.save(forceSave, null);
            this.world.savingDisabled = ex;
        } catch (ExceptionWorldConflict var2) {
            var2.printStackTrace();
        }

    }

    public boolean isAutoSave() {
        return !this.world.savingDisabled;
    }

    public void setAutoSave(boolean value) {
        this.world.savingDisabled = !value;
    }

    public Difficulty getDifficulty() {
        return Difficulty.getByValue(this.getHandle().difficulty);
    }

    public void setDifficulty(Difficulty difficulty) {
        this.getHandle().difficulty = difficulty.getValue();
    }

    public BlockMetadataStore getBlockMetadata() {
        return this.blockMetadata;
    }

    public boolean hasStorm() {
        return this.world.worldData.hasStorm();
    }

    public void setStorm(boolean hasStorm) {
        CraftServer server = this.world.getServer();
        WeatherChangeEvent weather = new WeatherChangeEvent(this, hasStorm);
        server.getPluginManager().callEvent(weather);
        if (!weather.isCancelled()) {
            this.world.worldData.setStorm(hasStorm);
            if (hasStorm) {
                this.setWeatherDuration(rand.nextInt(12000) + 12000);
            } else {
                this.setWeatherDuration(rand.nextInt(168000) + 12000);
            }
        }

    }

    public int getWeatherDuration() {
        return this.world.worldData.getWeatherDuration();
    }

    public void setWeatherDuration(int duration) {
        this.world.worldData.setWeatherDuration(duration);
    }

    public boolean isThundering() {
        return this.hasStorm() && this.world.worldData.isThundering();
    }

    public void setThundering(boolean thundering) {
        if (thundering && !this.hasStorm()) {
            this.setStorm(true);
        }

        CraftServer server = this.world.getServer();
        ThunderChangeEvent thunder = new ThunderChangeEvent(this, thundering);
        server.getPluginManager().callEvent(thunder);
        if (!thunder.isCancelled()) {
            this.world.worldData.setThundering(thundering);
            if (thundering) {
                this.setThunderDuration(rand.nextInt(12000) + 3600);
            } else {
                this.setThunderDuration(rand.nextInt(168000) + 12000);
            }
        }

    }

    public int getThunderDuration() {
        return this.world.worldData.getThunderDuration();
    }

    public void setThunderDuration(int duration) {
        this.world.worldData.setThunderDuration(duration);
    }

    public long getSeed() {
        return this.world.worldData.getSeed();
    }

    public boolean getPVP() {
        return this.world.pvpMode;
    }

    public void setPVP(boolean pvp) {
        this.world.pvpMode = pvp;
    }

    public void playEffect(Player player, Effect effect, int data) {
        this.playEffect(player.getLocation(), effect, data, 0);
    }

    public void playEffect(Location location, Effect effect, int data) {
        this.playEffect(location, effect, data, 64);
    }

    public <T> void playEffect(Location loc, Effect effect, T data) {
        this.playEffect(loc, effect, data, 64);
    }

    public <T> void playEffect(Location loc, Effect effect, T data, int radius) {
        if (data != null) {
            Validate.isTrue(data.getClass().equals(effect.getData()), "Wrong kind of data for this effect!");
        } else {
            Validate.isTrue(effect.getData() == null, "Wrong kind of data for this effect!");
        }

        if (data != null && data.getClass().equals(MaterialData.class)) {
            MaterialData datavalue1 = (MaterialData) data;
            Validate.isTrue(!datavalue1.getItemType().isBlock(), "Material must be block");
            this.spigot().playEffect(loc, effect, datavalue1.getItemType().getId(), datavalue1.getData(), 0.0F, 0.0F, 0.0F, 1.0F, 1, radius);
        } else {
            int datavalue = data == null ? 0 : CraftEffect.getDataValue(effect, data);
            this.playEffect(loc, effect, datavalue, radius);
        }

    }

    public void playEffect(Location location, Effect effect, int data, int radius) {
        this.spigot().playEffect(location, effect, data, 0, 0.0F, 0.0F, 0.0F, 1.0F, 1, radius);
    }

    public <T extends Entity> T spawn(Location location, Class<T> clazz) throws IllegalArgumentException {
        return this.spawn(location, clazz, SpawnReason.CUSTOM);
    }

    public FallingBlock spawnFallingBlock(Location location, org.bukkit.Material material, byte data) throws IllegalArgumentException {
        Validate.notNull(location, "Location cannot be null");
        Validate.notNull(material, "Material cannot be null");
        Validate.isTrue(material.isBlock(), "Material must be a block");
        double x = (double) location.getBlockX() + 0.5D;
        double y = (double) location.getBlockY() + 0.5D;
        double z = (double) location.getBlockZ() + 0.5D;
        EntityFallingBlock entity = new EntityFallingBlock(this.world, x, y, z, material.getId(), data);
        entity.c = 1;
        this.world.addEntity(entity, SpawnReason.CUSTOM);
        return (FallingBlock) entity.getBukkitEntity();
    }

    public FallingBlock spawnFallingBlock(Location location, int blockId, byte blockData) throws IllegalArgumentException {
        return this.spawnFallingBlock(location, org.bukkit.Material.getMaterial(blockId), blockData);
    }

    public <T extends Entity> T spawn(Location location, Class<T> clazz, SpawnReason reason) throws IllegalArgumentException {
        if (location != null && clazz != null) {
            net.minecraft.server.v1_5_R3.Entity entity = null;
            double x = location.getX();
            double y = location.getY();
            double z = location.getZ();
            float pitch = location.getPitch();
            float yaw = location.getYaw();
            if (Boat.class.isAssignableFrom(clazz)) {
                entity = new EntityBoat(this.world, x, y, z);
            } else if (FallingBlock.class.isAssignableFrom(clazz)) {
                x = (double) location.getBlockX();
                y = (double) location.getBlockY();
                z = (double) location.getBlockZ();
                int block = this.world.getTypeId((int) x, (int) y, (int) z);
                int face = this.world.getData((int) x, (int) y, (int) z);
                entity = new EntityFallingBlock(this.world, x + 0.5D, y + 0.5D, z + 0.5D, block, face);
            } else if (Projectile.class.isAssignableFrom(clazz)) {
                if (Snowball.class.isAssignableFrom(clazz)) {
                    entity = new EntitySnowball(this.world, x, y, z);
                } else if (Egg.class.isAssignableFrom(clazz)) {
                    entity = new EntityEgg(this.world, x, y, z);
                } else if (Arrow.class.isAssignableFrom(clazz)) {
                    entity = new EntityArrow(this.world);
                    entity.setPositionRotation(x, y, z, 0.0F, 0.0F);
                } else if (ThrownExpBottle.class.isAssignableFrom(clazz)) {
                    entity = new EntityThrownExpBottle(this.world);
                    entity.setPositionRotation(x, y, z, 0.0F, 0.0F);
                } else if (ThrownPotion.class.isAssignableFrom(clazz)) {
                    entity = new EntityPotion(this.world, x, y, z, CraftItemStack.asNMSCopy(new ItemStack(org.bukkit.Material.POTION, 1)));
                } else if (Fireball.class.isAssignableFrom(clazz)) {
                    if (SmallFireball.class.isAssignableFrom(clazz)) {
                        entity = new EntitySmallFireball(this.world);
                    } else if (WitherSkull.class.isAssignableFrom(clazz)) {
                        entity = new EntityWitherSkull(this.world);
                    } else {
                        entity = new EntityLargeFireball(this.world);
                    }

                    entity.setPositionRotation(x, y, z, yaw, pitch);
                    Vector block1 = location.getDirection().multiply(10);
                    ((EntityFireball) entity).setDirection(block1.getX(), block1.getY(), block1.getZ());
                }
            } else if (Minecart.class.isAssignableFrom(clazz)) {
                if (PoweredMinecart.class.isAssignableFrom(clazz)) {
                    entity = new EntityMinecartFurnace(this.world, x, y, z);
                } else if (StorageMinecart.class.isAssignableFrom(clazz)) {
                    entity = new EntityMinecartChest(this.world, x, y, z);
                } else if (ExplosiveMinecart.class.isAssignableFrom(clazz)) {
                    entity = new EntityMinecartTNT(this.world, x, y, z);
                } else if (HopperMinecart.class.isAssignableFrom(clazz)) {
                    entity = new EntityMinecartHopper(this.world, x, y, z);
                } else if (SpawnerMinecart.class.isAssignableFrom(clazz)) {
                    entity = new EntityMinecartMobSpawner(this.world, x, y, z);
                } else {
                    entity = new EntityMinecartRideable(this.world, x, y, z);
                }
            } else if (EnderSignal.class.isAssignableFrom(clazz)) {
                entity = new EntityEnderSignal(this.world, x, y, z);
            } else if (EnderCrystal.class.isAssignableFrom(clazz)) {
                entity = new EntityEnderCrystal(this.world);
                entity.setPositionRotation(x, y, z, 0.0F, 0.0F);
            } else if (LivingEntity.class.isAssignableFrom(clazz)) {
                if (Chicken.class.isAssignableFrom(clazz)) {
                    entity = new EntityChicken(this.world);
                } else if (Cow.class.isAssignableFrom(clazz)) {
                    if (MushroomCow.class.isAssignableFrom(clazz)) {
                        entity = new EntityMushroomCow(this.world);
                    } else {
                        entity = new EntityCow(this.world);
                    }
                } else if (Golem.class.isAssignableFrom(clazz)) {
                    if (Snowman.class.isAssignableFrom(clazz)) {
                        entity = new EntitySnowman(this.world);
                    } else if (IronGolem.class.isAssignableFrom(clazz)) {
                        entity = new EntityIronGolem(this.world);
                    }
                } else if (Creeper.class.isAssignableFrom(clazz)) {
                    entity = new EntityCreeper(this.world);
                } else if (Ghast.class.isAssignableFrom(clazz)) {
                    entity = new EntityGhast(this.world);
                } else if (Pig.class.isAssignableFrom(clazz)) {
                    entity = new EntityPig(this.world);
                } else if (!Player.class.isAssignableFrom(clazz)) {
                    if (Sheep.class.isAssignableFrom(clazz)) {
                        entity = new EntitySheep(this.world);
                    } else if (Skeleton.class.isAssignableFrom(clazz)) {
                        entity = new EntitySkeleton(this.world);
                    } else if (Slime.class.isAssignableFrom(clazz)) {
                        if (MagmaCube.class.isAssignableFrom(clazz)) {
                            entity = new EntityMagmaCube(this.world);
                        } else {
                            entity = new EntitySlime(this.world);
                        }
                    } else if (Spider.class.isAssignableFrom(clazz)) {
                        if (CaveSpider.class.isAssignableFrom(clazz)) {
                            entity = new EntityCaveSpider(this.world);
                        } else {
                            entity = new EntitySpider(this.world);
                        }
                    } else if (Squid.class.isAssignableFrom(clazz)) {
                        entity = new EntitySquid(this.world);
                    } else if (Tameable.class.isAssignableFrom(clazz)) {
                        if (Wolf.class.isAssignableFrom(clazz)) {
                            entity = new EntityWolf(this.world);
                        } else if (Ocelot.class.isAssignableFrom(clazz)) {
                            entity = new EntityOcelot(this.world);
                        }
                    } else if (PigZombie.class.isAssignableFrom(clazz)) {
                        entity = new EntityPigZombie(this.world);
                    } else if (Zombie.class.isAssignableFrom(clazz)) {
                        entity = new EntityZombie(this.world);
                    } else if (Giant.class.isAssignableFrom(clazz)) {
                        entity = new EntityGiantZombie(this.world);
                    } else if (Silverfish.class.isAssignableFrom(clazz)) {
                        entity = new EntitySilverfish(this.world);
                    } else if (Enderman.class.isAssignableFrom(clazz)) {
                        entity = new EntityEnderman(this.world);
                    } else if (Blaze.class.isAssignableFrom(clazz)) {
                        entity = new EntityBlaze(this.world);
                    } else if (Villager.class.isAssignableFrom(clazz)) {
                        entity = new EntityVillager(this.world);
                    } else if (Witch.class.isAssignableFrom(clazz)) {
                        entity = new EntityWitch(this.world);
                    } else if (Wither.class.isAssignableFrom(clazz)) {
                        entity = new EntityWither(this.world);
                    } else if (ComplexLivingEntity.class.isAssignableFrom(clazz)) {
                        if (EnderDragon.class.isAssignableFrom(clazz)) {
                            entity = new EntityEnderDragon(this.world);
                        }
                    } else if (Ambient.class.isAssignableFrom(clazz) && Bat.class.isAssignableFrom(clazz)) {
                        entity = new EntityBat(this.world);
                    }
                }

                if (entity != null) {
                    entity.setLocation(x, y, z, pitch, yaw);
                }
            } else if (Hanging.class.isAssignableFrom(clazz)) {
                Block block2 = this.getBlockAt(location);
                BlockFace face1 = BlockFace.SELF;
                if (block2.getRelative(EAST).getTypeId() == 0) {
                    face1 = EAST;
                } else if (block2.getRelative(NORTH).getTypeId() == 0) {
                    face1 = NORTH;
                } else if (block2.getRelative(WEST).getTypeId() == 0) {
                    face1 = WEST;
                } else if (block2.getRelative(SOUTH).getTypeId() == 0) {
                    face1 = SOUTH;
                }

                int dir;
                switch (face1) {
                    case SOUTH:
                    default:
                        dir = 0;
                        break;
                    case WEST:
                        dir = 1;
                        break;
                    case NORTH:
                        dir = 2;
                        break;
                    case EAST:
                        dir = 3;
                        break;
                }

                if (Painting.class.isAssignableFrom(clazz)) {
                    entity = new EntityPainting(this.world, (int) x, (int) y, (int) z, dir);
                } else if (ItemFrame.class.isAssignableFrom(clazz)) {
                    entity = new EntityItemFrame(this.world, (int) x, (int) y, (int) z, dir);
                }

                if (entity != null && !((EntityHanging) entity).survives()) {
                    entity = null;
                }
            } else if (TNTPrimed.class.isAssignableFrom(clazz)) {
                entity = new EntityTNTPrimed(this.world, x, y, z, null);
            } else if (ExperienceOrb.class.isAssignableFrom(clazz)) {
                entity = new EntityExperienceOrb(this.world, x, y, z, 0);
            } else if (Weather.class.isAssignableFrom(clazz)) {
                entity = new EntityLightning(this.world, x, y, z);
            } else if (!LightningStrike.class.isAssignableFrom(clazz)) {
                if (Fish.class.isAssignableFrom(clazz)) {
                    entity = new EntityFishingHook(this.world);
                    entity.setLocation(x, y, z, pitch, yaw);
                } else if (Firework.class.isAssignableFrom(clazz)) {
                    entity = new EntityFireworks(this.world, x, y, z, null);
                }
            }

            if (entity != null) {
                this.world.addEntity(entity, reason);
                return (T) entity.getBukkitEntity();
            } else {
                throw new IllegalArgumentException("Cannot spawn an entity for " + clazz.getName());
            }
        } else {
            throw new IllegalArgumentException("Location or entity class cannot be null");
        }
    }

    public ChunkSnapshot getEmptyChunkSnapshot(int x, int z, boolean includeBiome, boolean includeBiomeTempRain) {
        return CraftChunk.getEmptyChunkSnapshot(x, z, this, includeBiome, includeBiomeTempRain);
    }

    public void setSpawnFlags(boolean allowMonsters, boolean allowAnimals) {
        this.world.setSpawnFlags(allowMonsters, allowAnimals);
    }

    public boolean getAllowAnimals() {
        return this.world.allowAnimals;
    }

    public boolean getAllowMonsters() {
        return this.world.allowMonsters;
    }

    public int getMaxHeight() {
        return this.world.getHeight();
    }

    public int getSeaLevel() {
        return 64;
    }

    public boolean getKeepSpawnInMemory() {
        return this.world.keepSpawnInMemory;
    }

    public void setKeepSpawnInMemory(boolean keepLoaded) {
        this.world.keepSpawnInMemory = keepLoaded;
        ChunkCoordinates chunkcoordinates = this.world.getSpawn();
        int chunkCoordX = chunkcoordinates.x >> 4;
        int chunkCoordZ = chunkcoordinates.z >> 4;

        for (int x = -12; x <= 12; ++x) {
            for (int z = -12; z <= 12; ++z) {
                if (keepLoaded) {
                    this.loadChunk(chunkCoordX + x, chunkCoordZ + z);
                } else if (this.isChunkLoaded(chunkCoordX + x, chunkCoordZ + z)) {
                    if (this.getHandle().getChunkAt(chunkCoordX + x, chunkCoordZ + z) instanceof EmptyChunk) {
                        this.unloadChunk(chunkCoordX + x, chunkCoordZ + z, false);
                    } else {
                        this.unloadChunk(chunkCoordX + x, chunkCoordZ + z);
                    }
                }
            }
        }

    }

    public int hashCode() {
        return this.getUID().hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else if (this.getClass() != obj.getClass()) {
            return false;
        } else {
            CraftWorld other = (CraftWorld) obj;
            return this.getUID() == other.getUID();
        }
    }

    public File getWorldFolder() {
        return ((WorldNBTStorage) this.world.getDataManager()).getDirectory();
    }

    public void sendPluginMessage(Plugin source, String channel, byte[] message) {
        StandardMessenger.validatePluginMessage(this.server.getMessenger(), source, channel, message);
        Iterator i$ = this.getPlayers().iterator();

        while (i$.hasNext()) {
            Player player = (Player) i$.next();
            player.sendPluginMessage(source, channel, message);
        }

    }

    public Set<String> getListeningPluginChannels() {
        HashSet result = new HashSet();
        Iterator i$ = this.getPlayers().iterator();

        while (i$.hasNext()) {
            Player player = (Player) i$.next();
            result.addAll(player.getListeningPluginChannels());
        }

        return result;
    }

    public WorldType getWorldType() {
        return WorldType.getByName(this.world.getWorldData().getType().name());
    }

    public boolean canGenerateStructures() {
        return this.world.getWorldData().shouldGenerateMapFeatures();
    }

    public long getTicksPerAnimalSpawns() {
        return this.world.ticksPerAnimalSpawns;
    }

    public void setTicksPerAnimalSpawns(int ticksPerAnimalSpawns) {
        this.world.ticksPerAnimalSpawns = (long) ticksPerAnimalSpawns;
    }

    public long getTicksPerMonsterSpawns() {
        return this.world.ticksPerMonsterSpawns;
    }

    public void setTicksPerMonsterSpawns(int ticksPerMonsterSpawns) {
        this.world.ticksPerMonsterSpawns = (long) ticksPerMonsterSpawns;
    }

    public void setMetadata(String metadataKey, MetadataValue newMetadataValue) {
        this.server.getWorldMetadata().setMetadata(this, metadataKey, newMetadataValue);
    }

    public List<MetadataValue> getMetadata(String metadataKey) {
        return this.server.getWorldMetadata().getMetadata(this, metadataKey);
    }

    public boolean hasMetadata(String metadataKey) {
        return this.server.getWorldMetadata().hasMetadata(this, metadataKey);
    }

    public void removeMetadata(String metadataKey, Plugin owningPlugin) {
        this.server.getWorldMetadata().removeMetadata(this, metadataKey, owningPlugin);
    }

    public int getMonsterSpawnLimit() {
        return this.monsterSpawn < 0 ? this.server.getMonsterSpawnLimit() : this.monsterSpawn;
    }

    public void setMonsterSpawnLimit(int limit) {
        this.monsterSpawn = limit;
    }

    public int getAnimalSpawnLimit() {
        return this.animalSpawn < 0 ? this.server.getAnimalSpawnLimit() : this.animalSpawn;
    }

    public void setAnimalSpawnLimit(int limit) {
        this.animalSpawn = limit;
    }

    public int getWaterAnimalSpawnLimit() {
        return this.waterAnimalSpawn < 0 ? this.server.getWaterAnimalSpawnLimit() : this.waterAnimalSpawn;
    }

    public void setWaterAnimalSpawnLimit(int limit) {
        this.waterAnimalSpawn = limit;
    }

    public int getAmbientSpawnLimit() {
        return this.ambientSpawn < 0 ? this.server.getAmbientSpawnLimit() : this.ambientSpawn;
    }

    public void setAmbientSpawnLimit(int limit) {
        this.ambientSpawn = limit;
    }

    public void playSound(Location loc, Sound sound, float volume, float pitch) {
        if (loc != null && sound != null) {
            double x = loc.getX();
            double y = loc.getY();
            double z = loc.getZ();
            this.getHandle().makeSound(x, y, z, CraftSound.getSound(sound), volume, pitch);
        }
    }

    public String getGameRuleValue(String rule) {
        return this.getHandle().getGameRules().get(rule);
    }

    public boolean setGameRuleValue(String rule, String value) {
        if (rule != null && value != null) {
            if (!this.isGameRule(rule)) {
                return false;
            } else {
                this.getHandle().getGameRules().set(rule, value);
                return true;
            }
        } else {
            return false;
        }
    }

    public String[] getGameRules() {
        return this.getHandle().getGameRules().b();
    }

    public boolean isGameRule(String rule) {
        return this.getHandle().getGameRules().e(rule);
    }

    public void processChunkGC() {
        ++this.chunkGCTickCount;
        if (this.chunkLoadCount >= this.server.chunkGCLoadThresh && this.server.chunkGCLoadThresh > 0) {
            this.chunkLoadCount = 0;
        } else {
            if (this.chunkGCTickCount < this.server.chunkGCPeriod || this.server.chunkGCPeriod <= 0) {
                return;
            }

            this.chunkGCTickCount = 0;
        }

        ChunkProviderServer cps = this.world.chunkProviderServer;
        Iterator i$ = cps.chunks.values().iterator();

        while (i$.hasNext()) {
            net.minecraft.server.v1_5_R3.Chunk chunk = (net.minecraft.server.v1_5_R3.Chunk) i$.next();
            if (!this.isChunkInUse(chunk.x, chunk.z) && !cps.unloadQueue.contains(chunk.x, chunk.z)) {
                cps.queueUnload(chunk.x, chunk.z);
            }
        }

    }

    public Spigot spigot() {
        return this.spigot;
    }
}
