package org.bukkit.craftbukkit.v1_5_R3.chunkio;

/**
 * Created by Junhyeong Lim on 2017-06-15.
 */

import net.minecraft.server.v1_5_R3.Chunk;
import net.minecraft.server.v1_5_R3.ChunkRegionLoader;
import net.minecraft.server.v1_5_R3.NBTTagCompound;
import org.bukkit.Server;
import org.bukkit.craftbukkit.v1_5_R3.util.AsynchronousExecutor.CallBackProvider;
import org.bukkit.craftbukkit.v1_5_R3.util.LongHash;
import org.bukkit.event.world.ChunkLoadEvent;

import java.util.concurrent.atomic.AtomicInteger;

class ChunkIOProvider implements CallBackProvider<QueuedChunk, Chunk, Runnable, RuntimeException> {
    private final AtomicInteger threadNumber = new AtomicInteger(1);

    ChunkIOProvider() {
    }

    public Chunk callStage1(QueuedChunk queuedChunk) throws RuntimeException {
        ChunkRegionLoader loader = queuedChunk.loader;
        Object[] data = loader.loadChunk(queuedChunk.world, LongHash.msw(queuedChunk.coords), LongHash.lsw(queuedChunk.coords));
        if (data != null) {
            queuedChunk.compound = (NBTTagCompound) data[1];
            return (Chunk) data[0];
        } else {
            return null;
        }
    }

    public void callStage2(QueuedChunk queuedChunk, Chunk chunk) throws RuntimeException {
        if (chunk == null) {
            queuedChunk.provider.getChunkAt(LongHash.msw(queuedChunk.coords), LongHash.lsw(queuedChunk.coords));
        } else {
            int x = LongHash.msw(queuedChunk.coords);
            int z = LongHash.lsw(queuedChunk.coords);
            if (queuedChunk.provider.chunks.containsKey(queuedChunk.coords)) {
                queuedChunk.provider.unloadQueue.remove(queuedChunk.coords);
            } else {
                queuedChunk.loader.loadEntities(chunk, queuedChunk.compound.getCompound("Level"), queuedChunk.world);
                chunk.setLastSaved(queuedChunk.world.getTime());
                chunk.n = queuedChunk.provider.world.getTime();
                queuedChunk.provider.chunks.put(queuedChunk.coords, chunk);
                chunk.addEntities();
                if (queuedChunk.provider.chunkProvider != null) {
                    queuedChunk.provider.chunkProvider.recreateStructures(x, z);
                }

                Server server = queuedChunk.provider.world.getServer();
                if (server != null) {
                    server.getPluginManager().callEvent(new ChunkLoadEvent(chunk.bukkitChunk, false));
                }

                chunk.a(queuedChunk.provider, queuedChunk.provider, x, z);
            }
        }
    }

    public void callStage3(QueuedChunk queuedChunk, Chunk chunk, Runnable runnable) throws RuntimeException {
        runnable.run();
    }

    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable, "Chunk I/O Executor Thread-" + this.threadNumber.getAndIncrement());
        thread.setDaemon(true);
        return thread;
    }
}
