package org.spigotmc;

/**
 * Created by Junhyeong Lim on 2017-01-29.
 */

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.Arrays;
import java.util.List;

public class SpigotWorldConfig {
    private final String worldName;
    private final YamlConfiguration config;
    public int chunksPerTick;
    public int cactusModifier;
    public int caneModifier;
    public int melonModifier;
    public int mushroomModifier;
    public int pumpkinModifier;
    public int saplingModifier;
    public int wheatModifier;
    public double itemMerge;
    public double expMerge;
    public int viewDistance;
    public boolean antiXray = true;
    public int engineMode = 1;
    public List blocks = Arrays.asList(1, 5, 14, 15, 16, 21, 48, 49, 54, 56, 73, 74, 82, 129, 130);
    public AntiXray antiXrayInstance;
    public byte mobSpawnRange;
    public int animalActivationRange = 32;
    public int monsterActivationRange = 32;
    public int miscActivationRange = 16;
    public int playerTrackingRange = 48;
    public int animalTrackingRange = 48;
    public int monsterTrackingRange = 48;
    public int miscTrackingRange = 32;
    public int maxTrackingRange = 64;
    public int hopperTransfer = 8;
    public int hopperCheck = 8;
    public boolean randomLightUpdates;
    public int autoSavePeriod = -1;
    public int maxAutoSaveChunksPerTick = 24;
    private boolean verbose;

    public SpigotWorldConfig(String worldName) {
        this.worldName = worldName;
        this.config = SpigotConfig.config;
        this.init();
    }

    public void init() {
        this.verbose = this.getBoolean("verbose", true);
        this.log("-------- 월드 설정(속성) [" + this.worldName + "] --------");
        SpigotConfig.readConfig(SpigotWorldConfig.class, this);
    }

    private void log(String s) {
        if (this.verbose) {
            Bukkit.getLogger().info(s);
        }

    }

    private void autoSavePeriod() {
        autoSavePeriod = getInt("auto-save-interval", -1);
        if (autoSavePeriod > 0) {
            log("Auto Save Interval: " + autoSavePeriod + " (" + (autoSavePeriod / 20) + "s)");
        } else if (autoSavePeriod < 0) {
            autoSavePeriod = SpigotConfig.worldSavePeriod;
        }
    }

    private void maxAutoSaveChunksPerTick() {
        maxAutoSaveChunksPerTick = getInt("max-auto-save-chunks-per-tick", 24);
    }

    private boolean getBoolean(String path, boolean def) {
        this.config.addDefault("world-settings.default." + path, def);
        return this.config.getBoolean("world-settings." + this.worldName + "." + path, this.config.getBoolean("world-settings.default." + path));
    }

    private double getDouble(String path, double def) {
        this.config.addDefault("world-settings.default." + path, def);
        return this.config.getDouble("world-settings." + this.worldName + "." + path, this.config.getDouble("world-settings.default." + path));
    }

    private int getInt(String path, int def) {
        this.config.addDefault("world-settings.default." + path, def);
        return this.config.getInt("world-settings." + this.worldName + "." + path, this.config.getInt("world-settings.default." + path));
    }

    private <T> List getList(String path, T def) {
        this.config.addDefault("world-settings.default." + path, def);
        return this.config.getList("world-settings." + this.worldName + "." + path, this.config.getList("world-settings.default." + path));
    }

    private String getString(String path, String def) {
        this.config.addDefault("world-settings.default." + path, def);
        return this.config.getString("world-settings." + this.worldName + "." + path, this.config.getString("world-settings.default." + path));
    }

    private void chunksPerTick() {
        this.chunksPerTick = this.getInt("chunks-per-tick", 650);
        this.log("Tick 당 Grow 될 청크: " + this.chunksPerTick);
    }

    private void growthModifiers() {
        this.cactusModifier = this.getInt("growth.cactus-modifier", 100);
        this.log("선인장 성장: " + this.cactusModifier + "%");
        this.caneModifier = this.getInt("growth.cane-modifier", 100);
        this.log("사탕수수 성장: " + this.caneModifier + "%");
        this.melonModifier = this.getInt("growth.melon-modifier", 100);
        this.log("멜론 성장: " + this.melonModifier + "%");
        this.mushroomModifier = this.getInt("growth.mushroom-modifier", 100);
        this.log("버섯 성장: " + this.mushroomModifier + "%");
        this.pumpkinModifier = this.getInt("growth.pumpkin-modifier", 100);
        this.log("호박 성장: " + this.pumpkinModifier + "%");
        this.saplingModifier = this.getInt("growth.sapling-modifier", 100);
        this.log("나무 성장: " + this.saplingModifier + "%");
        this.wheatModifier = this.getInt("growth.wheat-modifier", 100);
        this.log("밀 성장: " + this.wheatModifier + "%");
    }

    private void itemMerge() {
        this.itemMerge = this.getDouble("merge-radius.item", 2.5D);
        this.log("아이템이 합쳐질 범위: " + this.itemMerge);
    }

    private void expMerge() {
        this.expMerge = this.getDouble("merge-radius.exp", 3.0D);
        this.log("경험치 볼이 합쳐질 범위: " + this.expMerge);
    }

    private void viewDistance() {
        this.viewDistance = this.getInt("view-distance", Bukkit.getViewDistance());
        this.log("시야 범위: " + this.viewDistance);
    }

    private void antiXray() {
        this.antiXray = this.getBoolean("anti-xray.enabled", this.antiXray);
        this.log("X-Ray 방지: " + this.antiXray);
        this.engineMode = this.getInt("anti-xray.engine-mode", this.engineMode);
        this.log("\t엔진 모드: " + this.engineMode);
        this.blocks = this.getList("anti-xray.blocks", this.blocks);
        this.log("\t블럭: " + this.blocks);
        this.antiXrayInstance = new AntiXray(this);
    }

    private void mobSpawnRange() {
        this.mobSpawnRange = (byte) this.getInt("mob-spawn-range", 4);
        this.log("몹 스폰 범위: " + this.mobSpawnRange);
    }

    private void activationRange() {
        this.animalActivationRange = this.getInt("entity-activation-range.animals", this.animalActivationRange);
        this.monsterActivationRange = this.getInt("entity-activation-range.monsters", this.monsterActivationRange);
        this.miscActivationRange = this.getInt("entity-activation-range.misc", this.miscActivationRange);
        this.log("엔티티 활성화 범위: An " + this.animalActivationRange + " / Mo " + this.monsterActivationRange + " / Mi " + this.miscActivationRange);
    }

    private void trackingRange() {
        this.playerTrackingRange = this.getInt("entity-tracking-range.players", this.playerTrackingRange);
        this.animalTrackingRange = this.getInt("entity-tracking-range.animals", this.animalTrackingRange);
        this.monsterTrackingRange = this.getInt("entity-tracking-range.monsters", this.monsterTrackingRange);
        this.miscTrackingRange = this.getInt("entity-tracking-range.misc", this.miscTrackingRange);
        this.maxTrackingRange = this.getInt("entity-tracking-range.other", this.maxTrackingRange);
        this.log("엔티티 추적 범위: Pl " + this.playerTrackingRange + " / An " + this.animalTrackingRange + " / Mo " + this.monsterTrackingRange + " / Mi " + this.miscTrackingRange + " / Other " + this.maxTrackingRange);
    }

    private void hoppers() {
        this.hopperCheck = this.getInt("ticks-per.hopper-check", this.hopperCheck);
        this.hopperTransfer = this.getInt("ticks-per.hopper-transfer", this.hopperTransfer);
        this.log("깔때기 이동: " + this.hopperTransfer + " Hopper Check: " + this.hopperCheck);
    }

    private void lightUpdates() {
        this.randomLightUpdates = this.getBoolean("random-light-updates", false);
        this.log("랜덤 조명 업데이트: " + this.randomLightUpdates);
    }
}
