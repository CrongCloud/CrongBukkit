package org.spigotmc.netty;

/**
 * Created by Junhyeong Lim on 2017-07-06.
 */
public enum ReadState {
    HEADER,
    DATA
}
