package org.spigotmc.netty;

import io.netty.channel.ChannelOption;
import io.netty.channel.socket.SocketChannel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;

/**
 * Created by Junhyeong Lim on 2017-07-06.
 */
public class NettySocketAdaptor extends Socket {
    private final SocketChannel ch;

    private NettySocketAdaptor(SocketChannel ch) {
        this.ch = ch;
    }

    public static NettySocketAdaptor adapt(SocketChannel ch) {
        return new NettySocketAdaptor(ch);
    }

    public void bind(SocketAddress bindpoint) throws IOException {
        this.ch.bind(bindpoint).syncUninterruptibly();
    }

    public synchronized void close() throws IOException {
        this.ch.close().syncUninterruptibly();
    }

    public void connect(SocketAddress endpoint) throws IOException {
        this.ch.connect(endpoint).syncUninterruptibly();
    }

    public void connect(SocketAddress endpoint, int timeout) throws IOException {
        this.ch.config().setConnectTimeoutMillis(timeout);
        this.ch.connect(endpoint).syncUninterruptibly();
    }

    public boolean equals(Object obj) {
        return obj instanceof NettySocketAdaptor && this.ch.equals(((NettySocketAdaptor) obj).ch);
    }

    public java.nio.channels.SocketChannel getChannel() {
        throw new UnsupportedOperationException("Operation not supported on Channel wrapper.");
    }

    public InetAddress getInetAddress() {
        return this.ch.remoteAddress().getAddress();
    }

    public InputStream getInputStream() throws IOException {
        throw new UnsupportedOperationException("Operation not supported on Channel wrapper.");
    }

    public boolean getKeepAlive() throws SocketException {
        return ((Boolean) this.ch.config().getOption(ChannelOption.SO_KEEPALIVE)).booleanValue();
    }

    public void setKeepAlive(boolean on) throws SocketException {
        this.ch.config().setOption(ChannelOption.SO_KEEPALIVE, Boolean.valueOf(on));
    }

    public InetAddress getLocalAddress() {
        return this.ch.localAddress().getAddress();
    }

    public int getLocalPort() {
        return this.ch.localAddress().getPort();
    }

    public SocketAddress getLocalSocketAddress() {
        return this.ch.localAddress();
    }

    public boolean getOOBInline() throws SocketException {
        throw new UnsupportedOperationException("Operation not supported on Channel wrapper.");
    }

    public void setOOBInline(boolean on) throws SocketException {
        throw new UnsupportedOperationException("Operation not supported on Channel wrapper.");
    }

    public OutputStream getOutputStream() throws IOException {
        throw new UnsupportedOperationException("Operation not supported on Channel wrapper.");
    }

    public int getPort() {
        return this.ch.remoteAddress().getPort();
    }

    public synchronized int getReceiveBufferSize() throws SocketException {
        return ((Integer) this.ch.config().getOption(ChannelOption.SO_RCVBUF)).intValue();
    }

    public synchronized void setReceiveBufferSize(int size) throws SocketException {
        this.ch.config().setOption(ChannelOption.SO_RCVBUF, Integer.valueOf(size));
    }

    public SocketAddress getRemoteSocketAddress() {
        return this.ch.remoteAddress();
    }

    public boolean getReuseAddress() throws SocketException {
        return ((Boolean) this.ch.config().getOption(ChannelOption.SO_REUSEADDR)).booleanValue();
    }

    public void setReuseAddress(boolean on) throws SocketException {
        this.ch.config().setOption(ChannelOption.SO_REUSEADDR, Boolean.valueOf(on));
    }

    public synchronized int getSendBufferSize() throws SocketException {
        return ((Integer) this.ch.config().getOption(ChannelOption.SO_SNDBUF)).intValue();
    }

    public synchronized void setSendBufferSize(int size) throws SocketException {
        this.ch.config().setOption(ChannelOption.SO_SNDBUF, Integer.valueOf(size));
    }

    public int getSoLinger() throws SocketException {
        return ((Integer) this.ch.config().getOption(ChannelOption.SO_LINGER)).intValue();
    }

    public synchronized int getSoTimeout() throws SocketException {
        throw new UnsupportedOperationException("Operation not supported on Channel wrapper.");
    }

    public synchronized void setSoTimeout(int timeout) throws SocketException {
        throw new UnsupportedOperationException("Operation not supported on Channel wrapper.");
    }

    public boolean getTcpNoDelay() throws SocketException {
        return ((Boolean) this.ch.config().getOption(ChannelOption.TCP_NODELAY)).booleanValue();
    }

    public void setTcpNoDelay(boolean on) throws SocketException {
        this.ch.config().setOption(ChannelOption.TCP_NODELAY, Boolean.valueOf(on));
    }

    public int getTrafficClass() throws SocketException {
        return ((Integer) this.ch.config().getOption(ChannelOption.IP_TOS)).intValue();
    }

    public void setTrafficClass(int tc) throws SocketException {
        this.ch.config().setOption(ChannelOption.IP_TOS, Integer.valueOf(tc));
    }

    public int hashCode() {
        return this.ch.hashCode();
    }

    public boolean isBound() {
        return this.ch.localAddress() != null;
    }

    public boolean isClosed() {
        return !this.ch.isOpen();
    }

    public boolean isConnected() {
        return this.ch.isActive();
    }

    public boolean isInputShutdown() {
        return this.ch.isInputShutdown();
    }

    public boolean isOutputShutdown() {
        return this.ch.isOutputShutdown();
    }

    public void sendUrgentData(int data) throws IOException {
        throw new UnsupportedOperationException("Operation not supported on Channel wrapper.");
    }

    public void setPerformancePreferences(int connectionTime, int latency, int bandwidth) {
        throw new UnsupportedOperationException("Operation not supported on Channel wrapper.");
    }

    public void setSoLinger(boolean on, int linger) throws SocketException {
        this.ch.config().setOption(ChannelOption.SO_LINGER, Integer.valueOf(linger));
    }

    public void shutdownInput() throws IOException {
        throw new UnsupportedOperationException("Operation not supported on Channel wrapper.");
    }

    public void shutdownOutput() throws IOException {
        this.ch.shutdownOutput().syncUninterruptibly();
    }

    public String toString() {
        return this.ch.toString();
    }
}
