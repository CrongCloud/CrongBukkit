package org.spigotmc.netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.handler.codec.EncoderException;
import net.minecraft.server.v1_5_R3.Packet;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Junhyeong Lim on 2017-07-06.
 */
public class PacketWriter {
    private final List<Packet> pending = new ArrayList<>();

    void release() {
        pending.clear();
    }

    void write(Channel channel, NettyNetworkManager networkManager, Packet msg) {
        pending.add(msg);

        int estimatedSize = 0;
        for (Packet packet : pending) {
            estimatedSize += packet.a();
        }

        ByteBuf outBuf = Unpooled.buffer(estimatedSize);

        try (DataOutputStream dataOut = new DataOutputStream(new ByteBufOutputStream(outBuf))) {
            for (Packet packet : pending) {
                outBuf.writeByte(packet.n());
                try {
                    packet.a(dataOut);
                } catch (IOException ex) {
                    throw new EncoderException(ex);
                }
            }
            networkManager.addWrittenBytes(outBuf.readableBytes());
            channel.writeAndFlush(outBuf);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            pending.clear();

            if (outBuf.refCnt() > 0) {
                outBuf.release();
            }
        }
    }
}
