package org.spigotmc.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import javax.crypto.Cipher;
import javax.crypto.ShortBufferException;

/**
 * Created by Junhyeong Lim on 2017-07-06.
 */
class CipherBase {
    private final Cipher cipher;
    private ThreadLocal<byte[]> heapInLocal = new CipherBase.EmptyByteThreadLocal();
    private ThreadLocal<byte[]> heapOutLocal = new CipherBase.EmptyByteThreadLocal();

    protected CipherBase(Cipher cipher) {
        this.cipher = cipher;
    }

    private byte[] bufToByte(ByteBuf in) {
        byte[] heapIn = this.heapInLocal.get();
        int readableBytes = in.readableBytes();
        if (heapIn.length < readableBytes) {
            heapIn = new byte[readableBytes];
            this.heapInLocal.set(heapIn);
        }

        in.readBytes(heapIn, 0, readableBytes);
        return heapIn;
    }

    protected ByteBuf cipher(ChannelHandlerContext ctx, ByteBuf in) throws ShortBufferException {
        int readableBytes = in.readableBytes();
        byte[] heapIn = this.bufToByte(in);
        ByteBuf heapOut = ctx.alloc().heapBuffer(this.cipher.getOutputSize(readableBytes));
        heapOut.writerIndex(this.cipher.update(heapIn, 0, readableBytes, heapOut.array(), heapOut.arrayOffset()));
        return heapOut;
    }

    protected void cipher(ByteBuf in, ByteBuf out) throws ShortBufferException {
        int readableBytes = in.readableBytes();
        byte[] heapIn = this.bufToByte(in);
        byte[] heapOut = (byte[]) this.heapOutLocal.get();
        int outputSize = this.cipher.getOutputSize(readableBytes);
        if (heapOut.length < outputSize) {
            heapOut = new byte[outputSize];
            this.heapOutLocal.set(heapOut);
        }

        out.writeBytes(heapOut, 0, this.cipher.update(heapIn, 0, readableBytes, heapOut));
    }

    private static class EmptyByteThreadLocal extends ThreadLocal<byte[]> {
        private EmptyByteThreadLocal() {
        }

        protected byte[] initialValue() {
            return new byte[0];
        }
    }
}
