package org.spigotmc;

import net.minecraft.server.v1_5_R3.EntityPlayer;
import net.minecraft.server.v1_5_R3.MinecraftServer;
import net.minecraft.server.v1_5_R3.Packet255KickDisconnect;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.io.File;
import java.util.Iterator;

public class RestartCommand extends Command {
    public RestartCommand(String name) {
        super(name);
        this.description = "Restarts the server";
        this.usageMessage = "/restart";
        this.setPermission("bukkit.command.restart");
    }

    public static void restart() {
        try {
            final File ex = new File(SpigotConfig.restartScript);
            if (!ex.isFile()) {
                System.out.println("스크립트 \'" + SpigotConfig.restartScript + "\' 를 찾을 수 없어 서버를 종료합니다. spigot.yml 의 restart-script 에서 경로를 수정해주세요.");
                Thread.sleep(5000);
            } else {
                System.out.println("스크립트 " + SpigotConfig.restartScript + " 로 재시작을 시도합니다.");
                Iterator shutdownHook = MinecraftServer.getServer().getPlayerList().players.iterator();

                while (true) {
                    if (!shutdownHook.hasNext()) {
                        try {
                            Thread.sleep(100L);
                        } catch (InterruptedException var5) {
                        }

                        MinecraftServer.getServer().ae().a();

                        try {
                            Thread.sleep(100L);
                        } catch (InterruptedException var4) {
                        }

                        try {
                            MinecraftServer.getServer().stop();
                        } catch (Throwable var3) {
                        }

                        Thread shutdownHook1 = new Thread() {
                            public void run() {
                                try {
                                    String e = System.getProperty("os.name").toLowerCase();
                                    if (e.contains("win")) {
                                        Runtime.getRuntime().exec("cmd /c start " + ex.getPath());
                                    } else {
                                        Runtime.getRuntime().exec(new String[]{"sh", ex.getPath()});
                                    }
                                } catch (Exception var2) {
                                    var2.printStackTrace();
                                }

                            }
                        };
                        shutdownHook1.setDaemon(true);
                        Runtime.getRuntime().addShutdownHook(shutdownHook1);
                        break;
                    }

                    EntityPlayer p = (EntityPlayer) shutdownHook.next();
                    p.playerConnection.networkManager.queue(new Packet255KickDisconnect("서버가 재시작됩니다."));
                    p.playerConnection.networkManager.d();
                }
            }

            System.exit(0);
        } catch (Exception var6) {
            var6.printStackTrace();
        }

    }

    public boolean execute(CommandSender sender, String currentAlias, String[] args) {
        if (this.testPermission(sender)) {
            restart();
        }

        return true;
    }
}